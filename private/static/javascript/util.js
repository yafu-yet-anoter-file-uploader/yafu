/******************************************************************************
 * Utility Library for YAFU                                                   *
 ******************************************************************************/

/**
 * toggles the visiblity of another object
 * 
 * @param The object to toggle
 * @param The object which toggles
 */
function toggleOptions(toggle, toggler) {
	
	if (toggle.style.display == 'none') {
		toggle.style.display = 'block';
		
		toggler.getElementsByTagName("span")[0].className = 'icToggleMinus';
	}
	else {
		toggle.style.display = 'none';
		
		toggler.getElementsByTagName("span")[0].className = 'icTogglePlus';
	}
	
}

/**
 * Updates a progress bar
 * 
 * @param Progress-Bar-Object
 * @param percentage (0...100)
 */
function update_progress_bar(progressBar, percentage) {
	
	if (progressBar) {
		var bar      = progressBar.getElementsByTagName("div")[0];
		var progress = progressBar.getElementsByTagName("div")[1];
		
		var totalWidth = progressBar.offsetWidth;
		
		bar.style.width = ((percentage / 100) * totalWidth) + "px";
		
		progress.innerHTML = Math.round(percentage) + ' %';
	}
	
}

/**
 * 
 */
var xhrObject = function() {
	
	var xmlHttp = null;
	try {
		// Mozilla, Opera, Safari sowie Internet Explorer (ab v7)
		xmlHttp = new XMLHttpRequest();
	}
	catch(e) {
		try {
			// MS Internet Explorer (ab v6)
			xmlHttp  = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			try {
				// MS Internet Explorer (ab v5)
				xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch(e) {
				xmlHttp  = null;
			}
		}
	}
	
	return xmlHttp;
}

/**
 * Timer Callback for Uploads
 */
var globalBlock = false;
function uploadProgress(id) {
	
	//update_progress_bar(globalProgressBar, globalProgress);
	
	if (globalBlock) {
		return;
	}
	
	globalBlock = true;
	
	var request = new xhrObject();
	
	if (request) {
		
		request.open('GET', '/upload/status/' + id, true);
		
		request.onreadystatechange = function () {
			if (request.readyState == 4) {
				
				globalBlock = false;
				
				var response = request.responseText.parseJSON();
				
				if (response.current && response.total) {
					var progress = (response.current / response.total * 100);
					update_progress_bar(document.getElementById('progress' + id), progress);
				}
				
			}
			else {
				
				
				
			}
		};
		
		request.send(null);
		
	}
	
}

/**
 * Shows the upload bar
 * 
 * @param form object
 * @param upload button area object
 * @param progress bar object
 * @param option toggle
 * @param option toggler
 * @param upload entry
 * @param intro text
 */
function upload(form, uploadButtonArea, progressBar, toggle, toggler, entry, text) {
	
	uploadButtonArea.style.display = "none";
	toggle.style.display = "none";
	toggler.style.display = "none";
	entry.style.display = "none";
	text.style.display = "none";
	progressBar.style.display = "block";
	
	id = form.getElementsByTagName('input')[0].value;
	progressBar.id = 'progress' + id;
	
	update_progress_bar(progressBar, 0);
	
	uploadProgress();
	var timer = window.setInterval("uploadProgress('" + id + "')", 500);
	
	form.target = 'upload_target';
	
}


/**
 * IE6 compatible iFrame Reloader -.-
 * 
 */
function iframeReloader(iframe) {
	
	var adress = '';
	
	if (iframe.contentWindow.location.href) {
		adress = iframe.contentWindow.location.href;
	}
	else if(iframe.contentDocument.location.href) {
		adress = iframe.contentDocument.location.href;
	}
	
	if (adress != 'about:blank') {
		window.location = adress;
	}
	
};