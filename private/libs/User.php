<?php

class ExceptionLoginIncorrect extends Exception {}
class ExceptionUserDoesNotExist extends Exception {}
class ExceptionUserIsBanned extends Exception {}
class ExceptionRegistrationFailed extends Exception {}

class User {
	
	// License Constants
	const LICENSE_USER_LOGIN =    1;
	
	// Attributes
	public    $id;
	public    $username;
	protected $license;
	public    $secretQuestion;
	public    $secretAnswer;
	
	/**
	 * constructs the object
	 *
	 * @param int User-ID
	 */
	public function __construct($userId) {
		
		if ($userId == -1) {
			
			return;
			
		}
		
		$sql = '
			SELECT
					`username`, `license`, `secretQuestion`, `secretAnswer`
				FROM
					`user`
				WHERE
					`id` = "' . intval($userId) . '"
		';
		$row = $GLOBALS['db']->query($sql)->getRow();
		
		if ($row === false) {
			
			throw new ExceptionUserDoesNotExist();
			
		}
		
		$this->id             = intval($userId);
		$this->username       = $row['username'];
		$this->license        = $row['license'];
		$this->secretQuestion = $row['secretQuestion'];
		$this->secretAnswer   = $row['secretAnswer'];
		
	}
	
	
	/**
	 * does check whether the user is able to login or not
	 * 
	 * @param string username
	 * @param string password
	 * @throws ExceptionLoginIncorrect
	 * @throws ExceptionUserDoesNotExist
	 * @throws ExceptionUserIsBanned
	 */
	public static function authenticate($username, $password) {
		
		$sql = '
			SELECT
					`id`, `password`, `license`
				FROM
					`user`
				WHERE
					`username` = "' . $GLOBALS['db']->escapeString($username) . '"
		';
		$result = $GLOBALS['db']->query($sql);
		
		if (!($row = $result->getRow())) {
			throw new ExceptionUserDoesNotExist();
		}
		
		if (!($row['license'] & User::LICENSE_USER_LOGIN)) {
			throw new ExceptionUserIsBanned();
		}
		
		if (md5($password) != $row['password']) {
			throw new ExceptionLoginIncorrect();
		}
		
		return $row['id'];
		
	}
	
	/**
	 * 
	 */
	public function checkPassword($password) {
		
		$sql = '
			SELECT
					`id`
				FROM
					`user`
				WHERE
					`id` = ' . $this->id . ' AND
					`password` = "' . md5($password) . '"
		';
		$result = $GLOBALS['db']->query($sql)->getRow();
		
		if ($result !== false) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * 
	 */
	public function setPassword($password) {
		
		$sql = '
			UPDATE
					`user`
				SET
					`password` = "' . md5($password) . '"
				WHERE
					`id` = ' . $this->id . '
		';
		$result = $GLOBALS['db']->query($sql);
		
	}
	
	/**
	 * 
	 */
	public function setSecret($secretQuestion, $secretAnswer) {
		
		$sql = '
			UPDATE
					`user`
				SET
					`secretQuestion` = ' . (trim($secretQuestion) != '' ? '"' . $GLOBALS['db']->escapeString($secretQuestion) . '"' : 'NULL') . ',
					`secretAnswer` = ' . (trim($secretAnswer) != '' ? '"' . $GLOBALS['db']->escapeString($secretAnswer) . '"' : 'NULL') . '
				WHERE
					`id` = ' . $this->id . '
		';
		$result = $GLOBALS['db']->query($sql);
		
	}
	
	
	/**
	 * 
	 * @param string desired username
	 * @param string desired passwort
	 * @param string desired passwort (repetition)
	 * @param string secret question (may be empty)
	 * @param string secret answer (may be empty)
	 */
	public static function create($username, $password, $passwordRepetition, $secretQuestion, $secretAnswer) {
		
		if ((trim($username) == '') || (trim($password) == '')) {
			throw new ExceptionRegistrationFailed('Empty Password');
		}
		
		if ($password != $passwordRepetition) {
			throw new ExceptionRegistrationFailed('Unequal Password');
		}
		
		if (trim($secretAnswer) == '') {
			$secretQuestion = '';
		}
		
		$sql = '
			SELECT
					`id`
				FROM
					`user`
				WHERE
					`username` = "' . $GLOBALS['db']->escapeString($username) . '"
		';
		$result = $GLOBALS['db']->query($sql)->getRow();
		
		if ($result !== false) {
			throw new ExceptionRegistrationFailed('Username exists already');
		}
		
		$sql = '
			INSERT
				INTO
					`user`
				SET
					`username` = "' . $GLOBALS['db']->escapeString($username) . '",
					`password` = "' . md5($password) . '",
					`secretQuestion` = ' . (trim($secretQuestion) != '' ? '"' . $GLOBALS['db']->escapeString($secretQuestion) . '"' : '""') . ',
					`secretAnswer` = ' . (trim($secretAnswer) != '' ? '"' . $GLOBALS['db']->escapeString($secretAnswer) . '"' : '""') . ',
					`creationTimestamp` = UNIX_TIMESTAMP(),
					`license` = 1
		';
		$result = $GLOBALS['db']->query($sql);
		
		return $result->lastInsertId;
		
	}
	
	/**
	 * get user by nickname
	 * 
	 * @param nickname
	 * @return object
	 */
	public static function getUserByNickname($nickname) {
		
		$sql = '
			SELECT
					`id`
				FROM
					`user`
				WHERE
					`username` = "' . $GLOBALS['db']->escapeString($nickname) . '"
		';
		$result = $GLOBALS['db']->query($sql)->getRow();
		
		if ($result === false) {
			
			throw new ExceptionUserDoesNotExist();
			
		}
		else {
			
			return new User($result['id']);
			
		}
		
	}
	
	
	/**
	 * delete a user
	 * 
	 * @param bool delete with files
	 */
	public function delete($deleteFiles) {
		
		if ($deleteFiles) {
			
			$sql = '
				SELECT
						`id`
					FROM
						`file`
					WHERE
						`userId` = ' . $GLOBALS['User']->id . '
			';
			$result = $GLOBALS['db']->query($sql);
			
			while ($row = $result->getRow()) {
				
				$file = new File($row['id']);
				$file->delete();
				
			}
			
		}
		
		$sql = '
			DELETE
				FROM
					`user`
				WHERE
					`id` = ' . $GLOBALS['User']->id . '
		';
		$result = $GLOBALS['db']->query($sql);
		
	}
	
}

?>
