<?php

//require_once(__ROOT__ . '/ext/geshi/class.geshi.php');
require_once(__ROOT__ . '/ext/geshi/geshi.php');

class ExceptionFileUploadFailed extends Exception{}
class ExceptionFileInexistant extends Exception{}

class File {
	
	const ENTRIES_PER_PAGE = 10;
	
	const TICKET_EXPIRATION = 3600;
	
	const VISIBILITY_PRIVATE  = 0;
	const VISIBILITY_PUBLIC   = 1;
	
	const HIGHLIGHTING_BINARY     = -1;
	const HIGHLIGHTING_PLAINTEXT  =  0;
	const HIGHLIGHTING_BBCODE     =  1;
	const HIGHLIGHTING_MARKDOWN   =  2;
	const HIGHLIGHTING_C          =  3;
	const HIGHLIGHTING_CPP        =  4;
	const HIGHLIGHTING_CSS        =  5;
	const HIGHLIGHTING_DELPHI     =  6;
	const HIGHLIGHTING_DOXYGEN    =  7;
	const HIGHLIGHTING_HTML       =  8;
	const HIGHLIGHTING_JAVA       =  9;
	const HIGHLIGHTING_JAVASCRIPT = 10;
	const HIGHLIGHTING_PHP        = 11;
	const HIGHLIGHTING_QBASIC     = 12;
	const HIGHLIGHTING_SQL        = 13;
	const HIGHLIGHTING_VHDL       = 14;
	const HIGHLIGHTING_WEB3D      = 15;
	
	const DURABILITY_INFINITE  = -1;
	const DURABILITY_10MINUTES =  0;
	const DURABILITY_30MINUTES =  1;
	const DURABILITY_1HOUR     =  2;
	const DURABILITY_12HOURS   =  3;
	const DURABILITY_1DAY      =  4;
	const DURABILITY_7DAYS     =  5;
	const DURABILITY_30DAYS    =  6;
	const DURABILITY_365DAYS   =  7;
	
	public    $id;
	public    $user;
	public    $folder;
	public    $mime;
	public    $filename;
	public    $filesize;
	public    $creationTimestamp;
	public    $expireTimestamp;
	public    $visibility;
	public    $durability;
	public    $highlighting;
	
	public    $uploaderIp;
	public    $uploaderReferer;
	public    $uploaderAgent;
	
	public    $md5;
	public    $sha1;
	
	public    $hash;
	public    $meta;
	
	/**
	 * constructs the file-object
	 *
	 * @param integer id
	 * @throws ExceptionFileInexistant
	 */
	public function __construct($id) {
		
		$sql = '
			SELECT
					`userId`, `folderId`, `filename`, `filesize`, `mimetype`,
					`creationTimestamp`, `expireTimestamp`, `visibility`,
					`durability`, `highlighting`,
					`uploaderIp`, `uploaderReferer`, `uploaderAgent`,
					`md5`, `sha1`, `hash`
				FROM
					`file`
				WHERE
					`id` = ' . intval($id) . '
		';
		$row = $GLOBALS['db']->query($sql)->getRow();
		
		if ($row === false) {
			
			throw new ExceptionFileInexistant();
			
		}
		
		if (!file_exists(__ROOT__ . '/private/files/' . intval($id))) {
			
			$sql = '
				DELETE
					FROM
						`file`
					WHERE
						`id` = ' . intval($id) . '
			';
			$row = $GLOBALS['db']->query($sql);
			
			throw new ExceptionFileInexistant();
			
		}
		
		$this->id                     = $id;
		
		$this->user                   = ($row['userId'] !== null) ? new User($row['userId']) : null;
		$this->folder                 = ($row['folderId'] !== null) ? new Folder($row['folderId']) : null;
		$this->mime                   =  $row['mimetype'];
		$this->filename               =  $row['filename'];
		$this->filesize               =  $row['filesize'];
		$this->creationTimestamp      =  $row['creationTimestamp'];
		$this->expireTimestamp        =  $row['expireTimestamp'];
		$this->visibility             =  $row['visibility'];
		$this->durability             =  $row['durability'];
		$this->highlighting           =  $row['highlighting'];
		
		$this->uploaderIp             =  $row['uploaderIp'];
		$this->uploaderReferer        =  $row['uploaderReferer'];
		$this->uploaderAgent          =  $row['uploaderAgent'];
		
		$this->md5                    =  $row['md5'];
		$this->sha1                   =  $row['sha1'];
		
		$this->hash                   =  $row['hash'];
		
		if (($this->user == null) && ($this->durability == File::DURABILITY_INFINITE)) {
			
			// Fix that:
			$sql = '
				UPDATE
						`file`
					SET
						`expireTimestamp` = ' . ($this->expireTimestamp + File::getExpirationTimestamp(File::DURABILITY_30DAYS)) . ',
						`durability` = ' . File::DURABILITY_30DAYS . '
					WHERE
						`id` = ' . intval($id) . '
			';
			$row = $GLOBALS['db']->query($sql);
			
		}
		
	}
	
	/**
	 * gets an extension out from a filename
	 * 
	 * @param string filename
	 * @return string extension
	 */
	public static function getExtensionFromFilename($str) {
		
		$i = strrpos($str, '.');
		
		if (!$i) {
			return '';
		}
		
		$l = strlen($str) - $i;
		
		$ext = substr($str,$i+1,$l);
		
		return $ext;
		
	}
	
	/**
	 * Gets a preview of the document
	 * 
	 * Immediate output allowed
	 */
	public function getPreview() {
		
		$sourceFilename = __ROOT__ . '/private/files/' . $this->id;
		
		switch ($this->mime) {
			case 'audio/mp3':
			case 'audio/x-generic':
			case 'audio/x-wav':
				echo '<div class="taC">';
				echo '<audio width="768" height="72" controls preload src="http://' . $_SERVER['SERVER_NAME'] . '/download/' . $this->getDownloadTicket() . '">';
				echo   '<embed src="http://' . $_SERVER['SERVER_NAME'] . '/download/' . $this->getDownloadTicket() . '" width="768" height="72">';
				echo '</audio>';
				echo '</div>';
				echo '<p class="taC">';
				echo  '<a href="/help/preview">Warum sehe ich keine Vorschau?</a>';
				echo '</p>';
				break;
				break;
			
			case 'application/ogg':
			case 'video/mpeg':
			case 'video/x-generic':
			case 'video/mp4':
			case 'video/x-flv':
			case 'video/x-msvideo':
				echo '<div class="taC">';
				echo '<video width="768" height="360" controls preload src="http://' . $_SERVER['SERVER_NAME'] . '/download/' . $this->getDownloadTicket() . '">';
				echo   '<embed src="http://' . $_SERVER['SERVER_NAME'] . '/download/' . $this->getDownloadTicket() . '" width="768" height="360">';
				echo '</video>';
				echo '</div>';
				echo '<p class="taC">';
				echo  '<a href="/help/preview">Warum sehe ich keine Vorschau?</a>';
				echo '</p>';
				break;
			
			case 'text/x-csrc':
			case 'text/x-c++src':
			case 'text/x-csharp':
			case 'text/x-php':
			case 'text/x-sql':
			case 'text/plain':
				
				$file = file_get_contents($sourceFilename);
				
				$this->meta = array();
				$this->meta['Zeilen']     = intval(exec('wc -l ' . $sourceFilename));;
				$this->meta['Wörter']     = intval(exec('wc -w ' . $sourceFilename));;
				$this->meta['Buchstaben'] = intval(exec('wc -m ' . $sourceFilename));;
				
				switch ($this->highlighting) {
					case File::HIGHLIGHTING_BINARY:
						echo '<p class="preview">';
						echo nl2br(htmlspecialchars($file));
						echo '</p>';
						break;
						
					case File::HIGHLIGHTING_PLAINTEXT:
						echo '<p class="preview">';
						echo nl2br(htmlspecialchars($file));
						echo '</p>';
						break;
						
					case File::HIGHLIGHTING_BBCODE:
						
						$rLF = '(?s)(.*?)'; // Darf Linefeed enthalten
						$rNLF = '(.+?)';    // Darf kein Linefeed enthalten
						
						$text = $file;
						
						$text = htmlspecialchars(stripslashes($text));
						
						$text = preg_replace('/\[b\]' . $rLF . '\[\/b\]/i', '<b>\1</b>', $text);
						$text = preg_replace('/\[i\]' . $rLF . '\[\/i\]/i', '<i>\1</i>', $text);
						$text = preg_replace('/\[u\]' . $rLF . '\[\/u\]/i', '<u>\1</u>', $text);
						$text = preg_replace('/\[s\]' . $rLF . '\[\/s\]/i', '<s>\1</s>', $text);
						
						$text = preg_replace('/\[strong\]' . $rLF . '\[\/strong\]/i', '<strong>\1</strong>', $text);
						$text = preg_replace('/\[stroke\]' . $rLF . '\[\/stroke\]/i', '<s>\1</s>', $text);
						
						$text = preg_replace('/\[quote\]' . $rLF . '\[\/quote\]/i', '<blockquote>\1</blockquote>', $text);
						$text = preg_replace('/\[quote=' . $rNLF . '\]' . $rLF . '\[\/quote\]/i', '<p>\1 sagt:</p><blockquote>\2</blockquote>', $text);
						
						$text = preg_replace('/\[size=' . $rNLF . '\]' . $rLF . '\[\/size\]/i', '<span style=\"font-size: \1px\">\2</span>', $text);
						$text = preg_replace('/\[color=' . $rNLF . '\]' . $rLF . '\[\/color\]/i', '<span style=\"color: \1\">\2</span>', $text);
						$text = preg_replace('/\[bgcolor=' . $rNLF . '\]' . $rLF . '\[\/bgcolor\]/i', '<span style=\"background: \1\">\2</span>', $text);
						
						$text = preg_replace('/\[img\]http\:\/\/' . $rLF . '\[\/img\]/i', '<img src="http://\1">', $text);
						$text = preg_replace('/\[img\]' . $rLF . '\[\/img\]/i', '<img src="http://\1">', $text);
						
						$text = preg_replace('/\[url\]http\:\/\/' . $rLF . '\[\/url\]/i', '<a href="http://\1">\1</a>', $text);
						$text = preg_replace('/\[url=http\:\/\/' . $rLF . '\]' . $rLF . '\[\/url\]/i', '<a href="http://\1">\2</a>', $text);
						$text = preg_replace('/\[url\]' . $rLF . '\[\/url\]/i', '<a href="http://\1">\1</a>', $text);
						$text = preg_replace('/\[url=' . $rLF . '\]' . $rLF . '\[\/url\]/i', '<a href="http://\1">\2</a>', $text);
						
						$text = preg_replace('/\[left\]' . $rLF . '\[\/left\]/i', '<left>\1</left>', $text);
						$text = preg_replace('/\[right\]' . $rLF . '\[\/right\]/i', '<right>\1</right>', $text);
						$text = preg_replace('/\[center\]' . $rLF . '\[\/center\]/i', '<center>\1</center>', $text);
						
						$text = preg_replace('/\[br\]/i', '<br>', $text);
						
						$text = str_replace("\r\n\r\n", '</p><p>', $text);
						$text = str_replace("\r\n", '<br>', $text);
						
						$text = str_replace("\r", '', $text);
						$text = str_replace("\n", '', $text);
						
						echo '<div class="preview"><p>';
						echo $text;
						echo '</p></div>';
						break;
						
					case File::HIGHLIGHTING_MARKDOWN:
						
						echo '<div class="preview">';
						system('markdown ' . $sourceFilename);
						echo '</div>';
						break;
						
					case File::HIGHLIGHTING_C:
						$geshi = new GeSHi($file, 'c');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_CPP:
						$geshi = new GeSHi($file, 'cpp');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_CSS:
						$geshi = new GeSHi($file, 'css');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_DELPHI:
						$geshi = new GeSHi($file, 'delphi');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_DOXYGEN:
						$geshi = new GeSHi($file, 'doxygen');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_HTML:
						$geshi = new GeSHi($file, 'html');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_JAVA:
						$geshi = new GeSHi($file, 'java');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_JAVASCRIPT:
						$geshi = new GeSHi($file, 'javascript');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_PHP:
						// GeSHi PHP is broken
						$geshi = new GeSHi($file, 'php');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_QBASIC:
						$geshi = new GeSHi($file, 'qbasic');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_SQL:
						$geshi = new GeSHi($file, 'sql');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_VHDL:
						$geshi = new GeSHi($file, 'vhdl');
						echo $geshi->parse_code();
						break;
						
					case File::HIGHLIGHTING_WEB3D:
						$geshi = new GeSHi($file, 'web3d');
						echo $geshi->parse_code();
						break;
						
				}
				
				break;
			
			case 'image/gif':
			case 'image/jpeg':
			case 'image/png':
				
				$size = getimagesize($sourceFilename, $meta);
				
				$this->meta = array();
				
				$this->meta['Abmessungen'] = $size[0] . 'x' . $size[1] . ' Pixel';
				
				if ($this->mime == 'image/jpeg') {
					$exif = exif_read_data($sourceFilename);
				}
				
				if (isset($exif) && ($exif !== false)) {
					
					if (isset($exif['DateTime'])) {
						$this->meta['Aufnahmezeitpunkt'] = $exif['DateTime'];
					}
					
					if (isset($exif['Make'])) {
						$this->meta['Kamera-Hersteller'] = $exif['Make'];
					}
					
					if (isset($exif['Model'])) {
						$this->meta['Kamera-Modell'] = $exif['Model'];
					}
					
					if (isset($exif['ExposureTime'])) {
						$this->meta['Belichtungszeit'] = $exif['ExposureTime'] . ' Sekunden';
					}
					
					if (isset($exif['FNumber'])) {
						$this->meta['Blendenzahl'] = $exif['FNumber'];
					}
					
					
					
				}
				
				echo '<div class="taC">';
				echo '<img src="/preview/' . $this->hash . '">';
				echo '</div>';
				
				break;
			
			default:
				echo '<div class="taC">';
				echo '<img src="/preview/' . $this->hash . '">';
				echo '</div>';
				break;
			
		}
		
	}
	
	/**
	 * 
	 */
	public function makePreview() {
		
		switch ($this->mime) {
			
			case 'image/gif':
			case 'image/png':
			case 'image/jpeg':
				
				$sourceFilename = __ROOT__ . '/private/files/' . $this->id;
				$targetFilename = __ROOT__ . '/private/files/' . $this->id . '_preview';
				
				File::scaleImage($this->mime, $sourceFilename, $targetFilename, 360);
				
				$targetFilename = __ROOT__ . '/private/files/' . $this->id . '_forum';
				
				File::scaleImage($this->mime, $sourceFilename, $targetFilename, 128, true);
				break;
			
			default:
				
				$mime = str_replace('/', '-', $this->mime);
				
				if (file_exists(__ROOT__ . '/ext/oxygen icons/256x256/mimetypes/' . $mime . '.png')) {
					
					$sourceFilename = __ROOT__ . '/ext/oxygen icons/256x256/mimetypes/' . $mime . '.png';
					
				}
				else {
					
					$sourceFilename = __ROOT__ . '/ext/oxygen icons/256x256/mimetypes/unknown.png';
					
				}
				
				$targetFilename = __ROOT__ . '/private/files/' . $this->id . '_preview';
				
				File::scaleImage('image/png', $sourceFilename, $targetFilename, 128, true, true);
				
				$targetFilename = __ROOT__ . '/private/files/' . $this->id . '_forum';
				
				File::scaleImage('image/png', $sourceFilename, $targetFilename, 128, true, true);
				
				break;
			
		}
		
	}
	
	public function scaleImage($mime, $sourceFilename, $targetFilename, $newW, $info = FALSE, $noImage = FALSE) {
		
		$size = getimagesize($sourceFilename);
		
		$ratio = $size[1] / $size[0];
		
		$newH = intval($newW * $ratio);
		
		if ($mime == 'image/jpeg') {
			$fullImage = ImageCreateFromJPEG($sourceFilename);
		}
		elseif ($mime == 'image/png') {
			$fullImage = ImageCreateFromPNG($sourceFilename);
		}
		elseif ($mime == 'image/gif') {
			$fullImage = ImageCreateFromGIF($sourceFilename);
		}
		
		$previewImage = ImageCreateTrueColor($newW, $newH + ( $info == true ? 20 : 0));
		$transparent = imagecolorallocatealpha($previewImage, 255, 255, 255, 127);
		imagefilledrectangle($previewImage, 0, 0, $newW, $newH + ( $info == true ? 20 : 0), $transparent);
		
		imagealphablending($previewImage, false);
		imagesavealpha($previewImage,true);
		
		ImageCopyResampled($previewImage, $fullImage, 0 ,0 ,0 ,0, $newW, $newH, $size[0], $size[1]);
		
		if ($info) {
			
			$black = imagecolorallocate($previewImage, 0, 0, 0);
			$white = imagecolorallocate($previewImage, 255, 255, 255);
			
			imagefilledrectangle($previewImage, 0, $newH, $newW, $newH + 20, $black);
			
			if ($noImage) {
				$text = File::getNiceSize($this->filesize);;
			}
			else {
				$text = $size[0] . 'x' . $size[1] . ' Pixel';
			}
			imagestring($previewImage, 4, ($newW / 2) - (strlen($text) * imagefontwidth(4) / 2), $newH + 2, $text, $white);
			
		}
		
		imagepng($previewImage, $targetFilename);
		
	}
	
	/**
	 * Creates a download ticker
	 *
	 * @return string URL to the ticket
	 */
	public function getDownloadTicket() {
		
		$sql = '
			SELECT
					`ticketId`
				FROM
					`file-ticket`
				WHERE
					`fileId` = ' . $this->id . ' AND
					`ip` = ' . ip2long($_SERVER['REMOTE_ADDR']) . '
		';
		$result = $GLOBALS['db']->query($sql)->getRow();
		
		if ($result !== false) {
			
			$sql = '
				UPDATE
						`file-ticket`
					SET
						`requestTimestamp` = UNIX_TIMESTAMP()
					WHERE
						`ticketId` = "' . $GLOBALS['db']->escapeString($result['ticketId']) . '" AND
						`ip` = ' . ip2long($_SERVER['REMOTE_ADDR']) . '
			';
			$GLOBALS['db']->query($sql);
			
			
			return $result['ticketId'];
			
		}
		
		// ---------------------------------------------------------------------
		
		$charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$found   = false;
		
		while (!$found) {
			
			$hash = '';
			while (strlen($hash) != 32) {
				$hash .= substr($charSet, rand(0, strlen($charSet)), 1);
			}
			
			$sql = '
				SELECT
						count(`ticketId`) as `count`
					FROM
						`file-ticket`
					WHERE
						`ticketId` = "' . $GLOBALS['db']->escapeString($hash) . '"
			';
			$result = $GLOBALS['db']->query($sql)->getRow();
			
			if ($result['count'] == 0) {
				
				$found = true;
				
			}
			
		}
		
		// ---------------------------------------------------------------------
		
		$sql = '
			REPLACE
					`file-ticket`
				SET
					`ticketId` = "' . $GLOBALS['db']->escapeString($hash) . '",
					`ip` = ' . ip2long($_SERVER['REMOTE_ADDR']) . ',
					`fileId` = ' . $this->id . ',
					`requestTimestamp` = UNIX_TIMESTAMP()
		';
		$result = $GLOBALS['db']->query($sql);
		
		$id = $result->lastInsertId;
		
		return $hash;
		
	}
	
	/**
	 * does the download
	 */
	public function download() {
		
		header('Content-Type: ' . $this->mime);
		header('Content-Length: ' . $this->filesize);
		header('Content-Disposition: attachment; filename="' . $this->filename . '"');
		
		readfile(__ROOT__ . '/private/files/' . $this->id);
		
	}
	
	/**
	 * downloads a file from a ticket
	 *
	 * @param ticketId
	 * @return object
	 * @throws ExceptionFileInexistant
	 */
	public static function getDownload($ticketId) {
		
		$sql = '
			SELECT
					`fileId`
				FROM
					`file-ticket`
				WHERE
					`ticketId` = "' . $GLOBALS['db']->escapeString($ticketId) . '" AND
					`ip` = ' . ip2long($_SERVER['REMOTE_ADDR']) . '
		';
		$result = $GLOBALS['db']->query($sql)->getRow();
		
		if ($result !== false) {
			
			return new File($result['fileId']);
			
		}
		else {
			
			throw new ExceptionFileInexistant();
			
		}
		
	}
	
	/**
	 * returns an object by giving a hash
	 * 
	 * @param string hash
	 */
	public static function getByHash($hash) {
		
		$sql = '
			SELECT
					`id`
				FROM
					`file`
				WHERE
					`hash` = "' . $GLOBALS['db']->escapeString($hash) . '"
		';
		$result = $GLOBALS['db']->query($sql)->getRow();
		
		if ($result !== false) {
			
			return new File($result['id']);
			
		}
		else {
			
			throw new ExceptionFileInexistant();
			
		}
		
	}
	
	/**
	 * 
	 * @param string mime
	 * @param string filename
	 */
	public static function mimeOverride($mime, $filename) {
		
		$ext = File::getExtensionFromFilename($filename);
		switch ($ext) {
			case 'c':
			case 'h':
				$mime = 'text/x-csrc';
				break;
			case 'cpp':
			case 'hpp':
				$mime = 'text/x-c++src';
				break;
			case 'cs':
				$mime = 'text/x-csharp';
				break;
			case 'mpg':
			case 'mpeg':
				$mime = 'video/x-generic';
				break;
			case 'mp1':
			case 'mp2':
			case 'mp3':
				$mime = 'audio/x-generic';
				break;
			case 'sql':
				$mime = 'text/x-sql';
				break;
			default:
				$mime = $mime;
		}
		
		return $mime;
		
	}
	
	/**
	 * creates a new file of from a temponary file
	 *
	 * @param string filename of source file
	 * @param string filename of temponary file
	 * @return object File-Object
	 * @throws ExceptionFileUploadFailed
	 */
	public static function create($filename, $tempFile, $visibility = File::VISIBILITY_PRIVATE, $durability = File::DURABILITY_30DAYS, $highlighting = File::HIGHLIGHTING_BINARY) {
		
		$mime = mime_content_type($tempFile);
		$size = filesize($tempFile);
		
		// Override for mime by extension
		$mime = file::mimeOverride($mime, $filename);
		
		$sha1 = substr(exec('sha1sum ' . $tempFile), 0, 40);
		$md5  = substr(exec('md5sum '  . $tempFile), 0, 32);
		
		// ---------------------------------------------------------------------
		
		if (isset($_SERVER['HTTP_REFERER'])) {
			
			$sql = '
				SELECT
						`id`
					FROM
						`userReferer`
					WHERE
						`referer` = "' . $GLOBALS['db']->escapeString($_SERVER['HTTP_REFERER']) . '"
			';
			$result = $GLOBALS['db']->query($sql)->getRow();
			
			if ($result !== FALSE) {
				
				$referer = $result['id'];
				
				$sql = '
					UPDATE
							`userReferer`
						SET
							`hits` = `hits` + 1,
							`lastHitTimestamp` = UNIX_TIMESTAMP()
						WHERE
							`id` = "' . $result['id'] . '"
				';
				$result = $GLOBALS['db']->query($sql);
				
			}
			else {
				
				$sql = '
					INSERT
						INTO
							`userReferer`
						SET
							`referer` = "' . $GLOBALS['db']->escapeString($_SERVER['HTTP_REFERER']) . '",
							`hits` = 1,
							`creationTimestamp` = UNIX_TIMESTAMP(),
							`lastHitTimestamp` = UNIX_TIMESTAMP()
				';
				$result = $GLOBALS['db']->query($sql);
				$referer = $result->lastInsertId;
				
			}
			
		}
		
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			
			$sql = '
				SELECT
						`id`
					FROM
						`userAgent`
					WHERE
						`agent` = "' . $GLOBALS['db']->escapeString($_SERVER['HTTP_USER_AGENT']) . '"
			';
			$result = $GLOBALS['db']->query($sql)->getRow();
			
			if ($result !== FALSE) {
				
				$agent = $result['id'];
				
				$sql = '
					UPDATE
							`userAgent`
						SET
							`hits` = `hits` + 1,
							`lastHitTimestamp` = UNIX_TIMESTAMP()
						WHERE
							`id` = "' . $result['id'] . '"
				';
				$result = $GLOBALS['db']->query($sql);
				
			}
			else {
				
				$sql = '
					INSERT
						INTO
							`userAgent`
						SET
							`agent` = "' . $GLOBALS['db']->escapeString($_SERVER['HTTP_USER_AGENT']) . '",
							`hits` = 1,
							`creationTimestamp` = UNIX_TIMESTAMP(),
							`lastHitTimestamp` = UNIX_TIMESTAMP()
				';
				$result = $GLOBALS['db']->query($sql);
				$agent = $result->lastInsertId;
				
			}
			
		}
		
		
		// ---------------------------------------------------------------------
		
		$charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$found   = false;
		
		while (!$found) {
			
			$hash = '';
			while (strlen($hash) != 40) {
				$hash .= substr($charSet, rand(0, strlen($charSet)), 1);
			}
			
			$sql = '
				SELECT
						count(`id`) as `count`
					FROM
						`file`
					WHERE
						`hash` = "' . $GLOBALS['db']->escapeString($hash) . '"
			';
			$result = $GLOBALS['db']->query($sql)->getRow();
			
			if ($result['count'] == 0) {
				
				$found = true;
				
			}
			
		}
		
		// ---------------------------------------------------------------------
		
		$sql = '
			INSERT
				INTO
					`file`
				SET
					`userId`            = ' . (isset($GLOBALS['User']) ? $GLOBALS['User']->id : 'NULL' ) . ',
					`folderId`          = NULL,
					`filename`          = "' . $GLOBALS['db']->escapeString($filename) . '",
					`filesize`          = ' . $size . ',
					`mimetype`          = "' . $GLOBALS['db']->escapeString($mime) . '",
					`creationTimestamp` = ' . time() . ',
					`expireTimestamp`   = ' . File::getExpirationTimestamp($durability) . ',
					`visibility`        = ' . intval($visibility) . ',
					`durability`        = ' . intval($durability) .  ',
					`highlighting`      = ' . intval($highlighting) .  ',
					`uploaderIp`        = ' . ip2long($_SERVER['REMOTE_ADDR']) . ',
					`uploaderReferer`   = ' . (isset($referer) ? $referer : 'NULL' ) . ',
					`uploaderAgent`     = ' . (isset($agent) ? $agent : 'NULL' ) . ',
					`sha1`              = "' . $GLOBALS['db']->escapeString($sha1) . '",
					`md5`               = "' . $GLOBALS['db']->escapeString($md5) . '",
					`hash`              = "' . $GLOBALS['db']->escapeString($hash) . '"
		';
		$result = $GLOBALS['db']->query($sql);
		
		$id = $result->lastInsertId;
		$target = __ROOT__ . '/private/files/' . $id;
		$copy = rename($tempFile, $target);
		
		if ($copy == FALSE) {
			throw new ExceptionFileUploadFailed('Copy failed');
		}
		
		return new File($id);
		
	}
	
	/**
	 * Calculates an expiration timestamp bias
	 * 
	 * @param string time interval
	 * @return int timestamp bias
	 */
	public static function getExpirationTimestamp($interval) {
		
		switch ($interval) {
			
			case File::DURABILITY_INFINITE:
				return -1;
				break;
			
			case File::DURABILITY_10MINUTES:
				return time() + (10 * 60);
				break;
			
			case File::DURABILITY_30MINUTES:
				return time() + (30 * 60);
				break;
			
			case File::DURABILITY_1HOUR:
				return time() + (1 * 3600);
				break;
			
			case File::DURABILITY_12HOURS:
				return time() + (12 * 3600);
				break;
			
			case File::DURABILITY_1DAY:
				return time() + (1 * 86400);
				break;
			
			case File::DURABILITY_7DAYS:
				return time() + (7 * 86400);
				break;
			
			case File::DURABILITY_30DAYS:
				return time() + (30 * 86400);
				break;
			
			case File::DURABILITY_365DAYS:
				return time() + (365 * 86400);
				break;
			
		}
		
		return 0;
	}
	
	/**
	 * Gets File Extension by MIME Type
	 * 
	 * @param string Mime Type
	 * @return string dot + extension
	 */
	public static function getExtension($mime) {
		
		switch ($mime) {
			
			case 'text/x-php':
				$extension = '.php';
				break;
			
			case 'text/x-c':
				$extension = '.c';
				break;
			
			case 'text/plain':
			default:
				$extension = '.txt';
				break;
			
		}
		
		return $extension;
		
	}
	
	/**
	 * Gets type of highlighting by MIME-Type
	 * 
	 * @param string Mime Type
	 * @return int Highlighting
	 */
	public static function getHighlite($mime) {
		
		switch ($mime) {
			
			case 'application/octet-stream':
				$highlighting = File::HIGHLIGHTING_BINARY;
				break;
			
			case 'text/x-php':
				$highlighting = File::HIGHLIGHTING_PHP;
				break;
			
			case 'text/x-sql':
				$highlighting = File::HIGHLIGHTING_SQL;
				break;
			
			case 'text/x-csrc':
				$highlighting = File::HIGHLIGHTING_C;
				break;
			
			case 'text/x-c++src':
				$highlighting = File::HIGHLIGHTING_CPP;
				break;
			
			case 'text/plain':
				$highlighting = File::HIGHLIGHTING_PLAINTEXT;
				break;
			
			default:
				$highlighting = File::HIGHLIGHTING_BINARY;
				break;
			
		}
		
		return $highlighting;
		
	}
	
	/**
	 * returns a nice human readable time
	 * 
	 * @param int filesize
	 * @return string filesize
	 */
	public static function getNiceSize($size) {
		
		if ($size >= 1073741824) {
			
			return number_format(($size / 1073741824), 2) . ' GB';
			
		}
		elseif ($size >= 1048576) {
			
			return number_format(($size / 1048576), 2) . ' MB';
			
		}
		elseif ($size >= 1024) {
			
			return number_format(($size / 1024), 2) . ' KB';
			
		}
		else {
			
			return $size . ' Byte';
			
		}
		
	}
	
	/**
	 * returns a nice human readable mime type
	 * 
	 * @param string mimetype
	 * @return string filetype
	 */
	public static function getNiceMime($mime) {
		
		switch ($mime) {
			
			case 'application/octet-stream':
				return 'Binärdaten-Stream';
				break;
			
			case 'application/ogg':
				return 'OGG-Container';
				break;
			
			case 'application/x-executable':
				return 'Anwendung';
				break;
			
			case 'application/x-gzip':
				return 'GZIP-Komprimierte Daten';
				break;
			
			case 'application/x-rar':
				return 'RAR-Komprimierte Daten';
				break;
			
			case 'application/x-sharedlib':
				return 'Programmbibliothek';
				break;
			
			case 'application/x-zip':
				return 'ZIP-Komprimierte Daten';
				break;
			
			case 'audio/x-generic':
				return 'Audiodatei';
				break;
			
			case 'image/jpeg':
				return 'JPEG-Bild';
				break;
			
			case 'image/gif':
				return 'GIF-Bild';
				break;
			
			case 'image/png':
				return 'PNG-Bild';
				break;
			
			case 'image/svg+xml':
				return 'SVG Vektorgrafik';
				break;
			
			case 'text/x-php':
				return 'PHP-Quelltext';
				break;
			
			case 'text/x-csrc':
				return 'C Quelltext';
				break;
			
			case 'text/x-c++src':
				return 'C++ Quelltext';
				break;
			
			case 'text/x-csharp':
				return 'C# Quelltext';
				break;
			
			case 'text/x-sql':
				return 'SQL-Dump';
				break;
			
			case 'text/plain':
				return 'Textdatei';
				break;
			
			case 'text/x-shellscript':
				return 'Shell-Script';
				break;
			
			case 'video/x-generic':
				return 'Videodatei';
				break;
			
			case 'video/mpeg':
				return 'MPEG-1 Videocontainer';
				break;
			
			case 'video/mpg2':
				return 'MPEG-2 Videocontainer';
				break;
			
			case 'video/mp4':
				return 'MPEG-4 Container';
				break;
			
			case 'video/x-flv':
				return 'Flash Video';
				break;
			
			case 'video/x-msvideo':
				return 'Videodatei für Microsoft-Systeme';
				break;
			
			default:
				return 'Binärdatei (' . $mime . ')';
				break;
			
		}
		
	}
	
	/**
	 * Search function
	 * 
	 * @param string query
	 * @param boolean search filenames
	 * @param boolean search content
	 * @param boolean search mime
	 * @param boolean search tags
	 * @param int page, starting with 0
	 * @return array array with objects
	 */
	public static function search($query, $filename, $content, $mime, $tags, $page = 0) {
		
		$files = array();
		
		if ($filename || $mime) {
			$sql = '
				SELECT
						`id`
					FROM
						`file`
					WHERE
						(
							(
								`visibility` = ' . FILE::VISIBILITY_PUBLIC . '
							)' . (isset($GLOBALS['User']) ? '
							OR
							(
								`visibility` = ' . FILE::VISIBILITY_PRIVATE . ' AND
								`userId` = ' . $GLOBALS['User']->id . '
							)' : '') . '
						)
						AND
						(
							' . ( $filename == true ? '`filename` LIKE "%' . $GLOBALS['db']->escapeString($query) . '%"' : '' ) . '
							' . ( $filename == true && $mime == true ? 'OR' : '') . '
							' . ( $mime == true ? '`mimetype` LIKE "%' . $GLOBALS['db']->escapeString($query) . '%"' : '' ) . '
						)
					ORDER BY
						`creationTimestamp` DESC
			';
			$result = $GLOBALS['db']->query($sql);
			
			$files['meta']['resultCount'] = $result->numRows;
			$files['meta']['pageCount'] = $result->numRows / File::ENTRIES_PER_PAGE;
			
			$sql .= '
					LIMIT
					' . $page * File::ENTRIES_PER_PAGE . ', ' . File::ENTRIES_PER_PAGE . '
			';
			$result = $GLOBALS['db']->query($sql);
			
			while ($row = $result->getRow()) {
				try {
					$files['files'][] = new File($row['id']);
				}
				catch (Exception $e) {
				}
			}
		}
		
		return $files;
		
	}
	
	/**
	 * Returns public and own files
	 * 
	 * @param int page number (starting with 0)
	 * @return array array with objects
	 */
	public static function browse($page = 0) {
		
		$files = array();
		
		$sql = '
			SELECT
					`id`
				FROM
					`file`
				WHERE
					(
						`visibility` = ' . FILE::VISIBILITY_PUBLIC . '
					)' . (isset($GLOBALS['User']) ? '
					OR
					(
						`visibility` = ' . FILE::VISIBILITY_PRIVATE . ' AND
						`userId` = ' . $GLOBALS['User']->id . '
					)' : '') . '
				ORDER BY
					`creationTimestamp` DESC
		';
		$result = $GLOBALS['db']->query($sql);
		
		$files['meta']['resultCount'] = $result->numRows;
		$files['meta']['pageCount'] = $result->numRows / File::ENTRIES_PER_PAGE;
		
		$sql .= '
				LIMIT
				' . $page * File::ENTRIES_PER_PAGE . ', ' . File::ENTRIES_PER_PAGE . '
		';
		$result = $GLOBALS['db']->query($sql);
		
		while ($row = $result->getRow()) {
			try {
				$files['files'][] = new File($row['id']);
			}
			catch (Exception $e) {
			}
		}
		
		return $files;
	}
	
	/**
	 * deletes the file
	 */
	public function delete() {
		
		$sql = '
			DELETE
				FROM
					`file`
				WHERE
					`id` = ' . $this->id . '
		';
		$result = $GLOBALS['db']->query($sql);
		
		$file = __ROOT__ . '/private/files/' . $this->id;
		unlink($file);
		
		if (file_exists($file . '_forum')) {
			unlink($file . '_forum');
		}
		
		if (file_exists($file . '_preview')) {
			unlink($file . '_preview');
		}
		
	}
	
	/**
	 * Selects expired files
	 */
	public static function getExpiredFiles() {
		
		$files = array();
		
		$sql = '
			SELECT
					`id`
				FROM
					`file`
				WHERE
					`expireTimestamp` < UNIX_TIMESTAMP() AND
					`expireTimestamp` != -1
		';
		$result = $GLOBALS['db']->query($sql);
		
		while ($row = $result->getRow()) {
			
			$files[] = new File($row['id']);
			
		}
		
		return $files;
		
	}
	
	/**
	 * Selects expired tickets
	 */
	public static function deleteExpiredTickets() {
		
		$tickets = array();
		
		$sql = '
			SELECT
					`ticketId`, `ip`, `fileId`
				FROM
					`file-ticket`
				WHERE
					(`requestTimestamp` + ' . File::TICKET_EXPIRATION . ') < UNIX_TIMESTAMP() 
		';
		$result = $GLOBALS['db']->query($sql);
		
		while ($row = $result->getRow()) {
			
			$file = new File($row['fileId']);
			
			$tickets[] = $row['ticketId'] . ' by ' . long2ip($row['ip']) . ' (' . $file->filename . ')';
			
		}
		
		$sql = '
			DELETE
				FROM
					`file-ticket`
				WHERE
					(`requestTimestamp` + ' . File::TICKET_EXPIRATION . ') < UNIX_TIMESTAMP() 
		';
		$result = $GLOBALS['db']->query($sql);
		
		return $tickets;
		
	}
}

?>
