<?php

class ExceptionDatabaseAlreadyConnected extends Exception {}
class ExceptionDatabaseUnableToConnect extends Exception {}
class ExceptionDatabaseInternalFailure extends Exception {}
class ExceptionDatabaseQueryFailed extends Exception {}

class Database {
	
	protected $connection = false;
	
	protected $hostname;
	protected $username;
	protected $password;
	protected $database;
	
	/**
	 * constructs the object
	 *
	 * @param string Hostname
	 * @param string Username
	 * @param string Password
	 * @param string Database
	 */
	public function __construct($hostname, $username, $password, $database) {
		
		$this->hostname = $hostname;
		$this->username = $username;
		$this->password = $password;
		$this->database = $database;
		
	}
	
	/**
	 * connects to the given MySQL-Server
	 *
	 * @throws ExceptionDatabaseAlreadyConnected
	 */
	public function connect() {
		
		if ($this->connection !== false) {
			
			throw new ExceptionDatabaseAlreadyConnected();
			
		}
		
		$this->connection = mysql_connect($this->hostname, $this->username, $this->password);
		
		// Check for whether the connection is still alive or not
		if (!mysql_ping($this->connection)) {
			
			$this->connection = mysql_connect( $myHostname, $myUsername, $myPassword  );
			
		}
		
		// Actually never reached because of the error handler but who cares,
		// its "clean coding".
		if (!$this->connection) {
			throw new ExceptionDatabaseUnableToConnect(mysql_error());
		}
		
		// Setup the database
		if (!mysql_select_db($this->database, $this->connection)) {
			throw new ExceptionDatabaseUnableToConnect(mysql_error($this->connection));
		}
		
		$this->query('SET NAMES "UTF8"');
		$this->query('SET CHARSET "UTF8"');
		
	}
	
	/**
	 * escape a string
	 *
	 * @param string
	 */
	public function escapeString($string) {
		
		return mysql_escape_string($string);
		
	}
	
	
	/**
	 * submits a query to the database
	 *
	 * @param string SQL-Query
	 * @throws ExceptionDatabaseAlreadyConnected
	 * @throws ExceptionDatabaseInternalFailure
	 */
	public function query($sql) {
		
		if ($this->connection === false) {
			
			$this->connect();
			
			if ($this->connection === false) {
				
				throw new ExceptionDatabaseInternalFailure();
				
			}
			
		}
		
		$result = mysql_query($sql);
		
		if (!$result) {
			
			throw new ExceptionDatabaseQueryFailed(mysql_error($this->connection));
			
		}
		
		$insertId = mysql_insert_id($this->connection);
		$affectedRows = mysql_affected_rows($this->connection);
		
		if (($result !== false) && ($result !== true) && ($result !== null)) {
			$rows = mysql_num_rows($result);
		}
		else {
			$rows = 0;
		}
		
		return new DatabaseResult($result, $this, $insertId, $affectedRows, $rows);
		
	}
}

?>
