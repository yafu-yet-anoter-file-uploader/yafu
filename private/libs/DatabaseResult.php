<?php

class DatabaseResult {
	
	protected $parent;
	protected $result;
	public $lastInsertId;
	public $affectedRows;
	public $numRows;
	
	/**
	 * constructs the object, should never been called by user
	 */
	public function __construct($result, $parent, $insertId, $affectedRows, $numRows) {
		
		$this->parent = $parent;
		$this->result = $result;
		
		$this->lastInsertId = $insertId;
		$this->affectedRows = $affectedRows;
		$this->numRows      = $numRows;
		
	}
	
	/**
	 * returns a result row as an associative array
	 *
	 * @return array or false
	 */
	public function getRow() {
		return mysql_fetch_assoc($this->result);
	}
}

?>
