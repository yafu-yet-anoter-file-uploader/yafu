<?php

class ExceptionNewsDoesNotExist extends Exception {};

class News {
	
	const ENTRIES_PER_PAGE = 5;
	
	private $id;
	public $title;
	public $author;
	public $timestamp;
	public $text;
	
	/**
	 * constructs a single object
	 * 
	 * @param int Id
	 */
	public function __construct($id) {
		
		$sql = '
			SELECT
					`creationTimestamp`, `author`, `title`, `text`
				FROM
					`news`
				WHERE
					`id` = ' . intval($id) . '
		';
		$row = $GLOBALS['db']->query($sql)->getRow();
		
		if ($row === false) {
			
			throw new ExceptionNewsDoesNotExist();
			
		}
		
		$this->title     = $row['title'];
		$this->author    = ($row['author'] !== null) ? new User($row['author']) : null;
		$this->timestamp = $row['creationTimestamp'];
		$this->text      = $row['text'];
		
	}
	
	/**
	 * gets a whole page
	 *
	 * @param int Page-Number
	 */
	public static function getPage($pageNumber) {
		
		$return = array();
		
		$sql = '
			SELECT
					`id`
				FROM
					`news`
				ORDER BY
					`creationTimestamp` DESC
				LIMIT
					' . ($pageNumber * News::ENTRIES_PER_PAGE) . ', ' . News::ENTRIES_PER_PAGE . '
		';
		$result = $GLOBALS['db']->query($sql);
		
		while ($row = $result->getRow()) {
			
			$return[] = new News($row['id']);
			
		}
		
		return $return;
		
	}
	
	/**
	 * gets the count of pages
	 */
	public static function getPageCount() {
		
		$sql = '
			SELECT
					count(`id`) AS `count`
				FROM
					`news`
		';
		$row = $GLOBALS['db']->query($sql)->getRow();
		
		
		return $row['count'] / News::ENTRIES_PER_PAGE;
		
	}
}

?>
