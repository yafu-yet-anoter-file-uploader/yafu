		<div id="content">
			<div class="flR">
				<?php require_once(__ROOT__ . '/private/template/user/loginWidget.php'); ?>
			</div>
			<div class="box mrR380">
				<h2>Durchsuchen</h2>
				<p>
					Dies ist eine Übersicht sowohl über deine selbst hochgeladenen Dateien (wenn du angemeldet bist) als auch über öffentliche Dateien.
				</p>
			</div>
			<?php if (isset($results['files']) && is_array($results['files'])) { ?>
			<table class="niceTable">
				<tr>
					<th>Dateiname</th>
					<th class="w200">Dateityp</th>
					<th class="w150">Dateigröße</th>
					<th class="w150">Uploader</th>
					<th class="w100">Sichtbarkeit</th>
					<th class="w150">Zeitpunkt</th>
				</tr>
				<?php foreach ($results['files'] as $result) { ?>
				<tr>
					<td><a href="/show/<?php echo $result->hash; ?>"><?php echo $result->filename; ?></a></td>
					<td><?php echo File::getNiceMime($result->mime); ?></td>
					<td><?php echo File::getNiceSize($result->filesize); ?></td>
					<td><?php echo ($result->user != NULL ? $result->user->username : 'Anonym'); ?></td>
					<td><?php echo ($result->visibility == File::VISIBILITY_PUBLIC ? 'Öffentlich' : 'Privat'); ?></td>
					<td><?php echo date('d.m.Y H:i:s', $result->creationTimestamp); ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="6">
						<div class="flR">
							<?php for ($i = 0; $i < $results['meta']['pageCount']; $i++) { ?>
								<?php if (($i != 0) && ($i != $results['meta']['pageCount'])) { echo ' &middot; '; } ?>
								<?php if ($i == $url[1]) { ?>
								<?php echo '<b>' . ($i+1) . '</b>'; ?>
								<?php } else { ?>
								<?php echo '<a href="/browse/' . $i . '">' . ($i+1) . '</a>'; ?>
								<?php } ?>
							<?php } ?>
						</div>
						<?php echo $results['meta']['resultCount']; ?> Dateien
					</td>
				</tr>
			</table>
			<?php } elseif (isset($results)) { ?>
			<p>
				Es wurden noch keine einsehbaren Dateien hochgeladen.
			</p>
			<?php } ?>
		</div>
