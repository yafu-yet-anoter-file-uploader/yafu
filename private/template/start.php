			<div id="content">
				<div class="flR">
					<?php require_once(__ROOT__ . '/private/template/user/loginWidget.php'); ?>
					<div class="box w350">
						<h2>Tag Cloud</h2>
						<p>
							Coming soon
						</p>
					</div>
				</div>
				<div class="box mrR380">
					<h2>Willkommen bei YAFU</h2>
					<p>
						<strong>YAFU</strong> steht für <i>Yet Another File Uploader</i> und ist ein unter der GPLv2 lizensiertes PHP-Skript für Filehoster und solche, die es werden wollen.
					</p>
					<p>
						Der Quelltext steht dabei via <a href="https://gitorious.org/yafu">Gitorious</a> zur Verfügung und darf gemäß der Bedingungen der GPLv2 kostenfrei verwendet und weitergegeben werden.
					</p>
					<p class="readmore">
						<a href="/info">mehr über YAFU</a>
					</p>
				</div>
				<div class="mrR380">
					<h3>Aktuelle Neuigkeiten <span class="more">(<a href="/news">mehr</a>)</span></h3>
				</div>
				<?php
				foreach ($newsEntries as $newsEntry) {
				?>
				<h4><?php echo htmlspecialchars($newsEntry->title); ?></h4>
				<p class="meta">
					<?php
					if ($newsEntry->author !== null) {
						echo 'am ' . date('d.m.Y H:i:s', $newsEntry->timestamp) . ' von <strong>' . $newsEntry->author->username .  '</strong>';
					}
					else {
						echo 'am ' . date('d.m.Y H:i:s', $newsEntry->timestamp);
					}
					?>
				</p>
				<p class="newsEntry">
					<?php echo $newsEntry->text; ?>
				</p>
				<?php
				}
				?>
			</div>
