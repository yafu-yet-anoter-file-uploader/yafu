		<div id="content">
			<div class="box">
				<h2>Über YAFU</h2>
				<p>
					Unter YAFU ("Yet Another File Uploader") war bislang ein PHP-Skript bekannt, das gegen 2007 veröffentlicht wurde, jedoch seitdem nicht mehr gepflegt wurde und schon längst nicht mehr bezogen werden kann.
					Dabei handelte es sich um ein Skript, mit dem jeder Webspace zum Filehoster werden konnte, inklusive Komfort-Funktionen wie Sichtbarkeit, NoPaste-Service etc pp.
					Dadurch, dass es im Jahre 2011 nicht mehr bezogen werden konnte, um es ggf. zu verändern, beschloss Philipp Müller, das Projekt zu reverse engineeren und fortzuführen, unter selbem Namen.
					Dieses Mal allerdings soll die Verfügbarkeit des Skriptes gewahrt bleiben - indem es bei <a href="https://gitorious.org/yafu">Gitorious</a> gehostet wird.
				</p>
				<p>
					YAFU bietet dabei viele Funktionen:
				</p>
				<ul>
					<li>Datei-Upload, inkl. Sonderfunktionen für Texte, Bilder, Musik & Videos</li>
					<li>Echtzeit-Suche</li>
					<li>Dateibrowser mit Ordner-Struktur für angemeldete Benutzer</li>
				</ul>
			</div>
		</div>
