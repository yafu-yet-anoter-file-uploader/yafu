			<div id="content">
				<div>
					<h3>News-Archiv</h3>
				</div>
				<?php
				foreach ($newsEntries as $newsEntry) {
				?>
				<h4><?php echo htmlspecialchars($newsEntry->title); ?></h4>
				<p class="meta">
					<?php
					if ($newsEntry->author !== null) {
						echo 'am ' . date('d.m.Y H:i:s', $newsEntry->timestamp) . ' von <strong>' . $newsEntry->author->username .  '</strong>';
					}
					else {
						echo 'am ' . date('d.m.Y H:i:s', $newsEntry->timestamp);
					}
					?>
				</p>
				<p class="newsEntry">
					<?php echo $newsEntry->text; ?>
				</p>
				<?php
				}
				?>
				
				<p>
					Seite
					<?php
					
					for ($i = 0; $i < $newsPageCount; $i++) {
						if (($i != 0) && ($i != $newsPageCount)) {
							echo '&middot; ';
						}
						
						if (intval($url[1]) == $i) {
							echo '<b>' . ($i+1) . '</b> ';
						}
						else {
							echo '<a href="/news/' . $i . '">' . ($i+1) . '</a> ';
						}
					}
					
					?>
				</p>
			</div>
