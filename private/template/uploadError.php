		<div id="content">
			<div class="flR">
				<?php require_once(__ROOT__ . '/private/template/user/loginWidget.php'); ?>
			</div>
			<div class="box mrR380">
				<h2>Datei hochladen</h2>
				<p>
					<strong>Fehler:</strong> Der Upload ist fehlgeschlagen. Versuche es noch einmal.
				</p>
				<form action="/upload/store" method="post" enctype="multipart/form-data" id="form" onsubmit="return upload(document.getElementById('form'), document.getElementById('uploadButtonArea'), document.getElementById('progress'), document.getElementById('toggle'), document.getElementById('toggler'), document.getElementById('file'), document.getElementById('text'));">
					<input type="hidden" name="APC_UPLOAD_PROGRESS" id="id" value="<?php echo uniqid(); ?>">
					<p id="text">
						Wähle eine Datei von deinem PC aus, die du mit anderen teilen möchtest.
					</p>
					<p id="file">
						<label>Datei:</label>
						<input type="file" name="file">
					</p>
					<div id="progress" class="progressBar" style="display: none;">
						<div class="progress">
						</div>
						<div class="percentage">
							0 %
						</div>
					</div>
					<p id="uploadButtonArea">
						<input type="submit" value="Hochladen">
					</p>
					<p>
						<a href="#" id="toggler" class="toggle" onclick="toggleOptions(document.getElementById('toggle'), document.getElementById('toggler'));">
							<span class="icTogglePlus">&nbsp;</span>
							Optionen
						</a>
					</p>
					<div style="display: none;" id="toggle">
						<p>
							<label class="w100 dIB">Sichtbarkeit:</label>
							<select class="w300" name="visibility">
								<option value="private">Privat</option>
								<option value="public" selected>Öffentlich</option>
							</select>
						</p>
						<p>
							<label class="w100 dIB">Haltbarkeit:</label>
							<select class="w300" name="durability">
								<option value="10m">10 Minuten</option>
								<option value="30m">30 Minuten</option>
								<option value="1h">1 Stunde</option>
								<option value="12h">12 Stunden</option>
								<option value="1d">1 Tag</option>
								<option value="7d">7 Tage</option>
								<option value="30d" selected>30 Tage</option>
								<option value="365d">365 Tage</option>
								<?php if (isset($GLOBALS['User'])) { ?><option value="inf">Unbegrenzt</option><?php } ?>
							</select>
						</p>
					</div>
					<iframe id="upload_target" name="upload_target" src="about:blank" style="display: none;"></iframe>
				</form>
			</div>
		</div>
