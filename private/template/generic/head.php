<?php

header('content-type: text/html; charset=utf-8');

?><!doctype html>
<html>
	<head>
		<title>YAFU<?php if (isset($title)) { ?> &middot; <?php echo htmlspecialchars($title); } ?></title>
		<link rel="stylesheet" type="text/css" href="/st/css/generic">
		<link rel="stylesheet" type="text/css" href="/st/css/icon">
		<link rel="stylesheet" type="text/css" href="/st/css/specific">
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<script src="/st/js/json"></script>
		<script src="/st/js/util"></script>
	</head>
	<body>
