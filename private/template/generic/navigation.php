		<div id="header">
			<div id="logo">
				<a href="/">
					<h1>
						YAFU
					</h1>
				</a>
			</div>
			<div id="navigation">
				<ul>
					<li>
						<a href="/start"<?php if (($url[0] == 'start') || ($url[0] == '')) { echo ' class="active"'; } ?>>
							Start
						</a>
					</li>
					<li>
						<a href="/upload"<?php if ($url[0] == 'upload') { echo ' class="active"'; } ?>>
							Hochladen
						</a>
					</li>
					<li>
						<a href="/paste"<?php if ($url[0] == 'paste') { echo ' class="active"'; } ?>>
							Einfügen
						</a>
					</li>
					<li>
						<a href="/search"<?php if ($url[0] == 'search') { echo ' class="active"'; } ?>>
							Suchen
						</a>
					</li>
					<li>
						<a href="/browse"<?php if ($url[0] == 'browse') { echo ' class="active"'; } ?>>
							Übersicht
						</a>
					</li>
					<li>
						<a href="/info"<?php if ($url[0] == 'info') { echo ' class="active"'; } ?>>
							Info
						</a>
					</li>
				</ul>
			</div>
		</div>
