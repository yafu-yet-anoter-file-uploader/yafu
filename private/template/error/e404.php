		<div id="content">
			<div class="box">
				<h2>Fehler 404: Datei nicht gefunden</h2>
				<p>
					Die angeforderte Datei konnte auf dem Server nicht gefunden haben.
				</p>
				<p>
					Versuchen Sie folgendes:
				</p>
				<ul>
					<li>
						Versuchen Sie es später erneut.
					</li>
					<li>
						Prüfen Sie die Adresse auf Rechtschreibfehler
					</li>
					<li>
						Bitten Sie den Besitzer der Datei, diese erneut hochzuladen.
					</li>
				</ul>
			</div>
		</div>

