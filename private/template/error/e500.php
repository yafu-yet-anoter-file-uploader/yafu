		<div id="content">
			<div class="box">
				<h2>Fehler 500: Kritischer Serverfehler</h2>
				<p>
					Es ist ein Fehler auf dem Server aufgetreten, der die Seitenausführung verhindert.<br>
					Der Serverbetreiber wurde über diesen Vorfall informiert.
				</p>
				<p>
					Versuchen Sie folgendes:
				</p>
				<ul>
					<li>
						Versuchen Sie es später erneut.
					</li>
					<li>
						Prüfen Sie die Adresse auf Rechtschreibfehler
					</li>
					<li>
						Gehen Sie mal wieder raus, an die frische Luft, dort draußen warten viel aufregendere Abenteuer.
					</li>
				</ul>
				<?php
				if ($type == 'error') {
					require_once(__ROOT__ . '/private/template/error/errorDebug.php');
				}
				?>
			</div>
		</div>
