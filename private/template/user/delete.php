			<div id="content">
				<h2>Account löschen</h2>
				<?php
				if (isset($notice)) {
					?>
					<div class="">
						<strong>Hinweis:</strong> <?php echo $notice; ?>
					</div>
					<?php
				}
				?>
				<?php
				if (isset($error)) {
					?>
					<div class="">
						<strong>Fehler:</strong> <?php echo $error; ?>
					</div>
					<?php
				}
				?>
				<form action="/user/delete" method="post">
					<p>
						<label>Passwort:</label>
						<input type="password" name="password" class="w100P" value="">
					</p>
					<p>
						<input type="checkbox" name="deleteFiles" id="deleteFiles" value="true">
						<label for="deleteFiles">Zusammen mit allen Dateien löschen</label>
					</p>
					<p>
						<a href="/user">&lt; Zurück</a> &middot; <input type="submit" value="Account löschen">
					</p>
				</form>
			</div>