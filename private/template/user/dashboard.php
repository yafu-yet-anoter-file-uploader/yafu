			<div id="content">
				<h2>Dashboard</h2>
				<ul>
					<li><a href="/user/changePassword">Passwort ändern</a></li>
					<li><a href="/user/changeSecret">Geheime Frage / Antwort ändern</a></li>
					<li><a href="/user/delete">Account löschen</a></li>
					<li><a href="/user/logout">Abmelden</a></li>
				</ul>
			</div>