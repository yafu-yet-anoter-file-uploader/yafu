			<div id="content">
				<div>
					<h3>Passwort vergessen</h3>
					<form action="/user/lostPassword" method="post">
						<?php
						if (isset($notice)) {
							?>
							<div class="">
								<strong>Hinweis:</strong> <?php echo $notice; ?>
							</div>
							<?php
						}
						?>
						<?php
						if (isset($error)) {
							?>
							<div class="">
								<strong>Fehler:</strong> <?php echo $error; ?>
							</div>
							<?php
						}
						?>
						<p>
							Der folgende Assistent wird Ihnen dabei helfen, wieder Zugriff auf ihren Account zu erlangen.<br>
							Bedenken Sie bitte, dass sie eine Sicherheitsfrage hinterlegt haben müssen, damit dieser Prozess erfolgreich verläuft.
						</p>
						<p>
							<label>Ihr Benutzername:</label>
						</p>
						<p>
							<input type="text" name="username" class="w100P" value="">
						</p>
						<p>
							<a href="/user">&lt; Zurück</a> &middot; <input type="submit" value="Weiter &gt;">
						</p>
					</form>
				</div>
			</div>
