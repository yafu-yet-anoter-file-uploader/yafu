			<div id="content">
				<h2>Passwort ändern</h2>
				<?php
				if (isset($notice)) {
					?>
					<div class="">
						<strong>Hinweis:</strong> <?php echo $notice; ?>
					</div>
					<?php
				}
				?>
				<?php
				if (isset($error)) {
					?>
					<div class="">
						<strong>Fehler:</strong> <?php echo $error; ?>
					</div>
					<?php
				}
				?>
				<form action="/user/changePassword" method="post">
					<p>
						<label>Bisheriges Passwort:</label>
						<input type="password" name="passwordOld" class="w100P" value="">
					</p>
					<p>
						<label>Passwort:</label>
						<input type="password" name="password" class="w100P" value="">
					</p>
					<p>
						<label>Passwort wiederholen:</label>
						<input type="password" name="passwordRepetition" class="w100P" value="">
					</p>
					<p>
						<a href="/user">&lt; Zurück</a> &middot; <input type="submit" value="Passwort ändern">
					</p>
				</form>
			</div>