			<div id="content">
				<div>
					<h3>Passwort vergessen</h3>
					<form action="/user" method="post">
						<p>
							Sie haben sich erfolgreich authentifiziert und wurden in den Account eingeloggt.<br>
							Ihr neues Passwort lautet <strong><?php echo $newPassword; ?></strong>. Bitte ändern Sie es umgehend.
						</p>
						<p>
							<a href="/user">&lt; Zurück</a> &middot; <input type="submit" value="Weiter &gt;">
						</p>
					</form>
				</div>
			</div>
