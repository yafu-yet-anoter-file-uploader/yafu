		<div id="content">
			<div class="flR">
				<?php require_once(__ROOT__ . '/private/template/user/loginWidget.php'); ?>
			</div>
			<div class="box mrR380">
				<h2>Fehler: Ungültige Anmeldung</h2>
				<p>
					Sie konnten nicht am System authentifiziert werden. Stellen Sie sicher, dass ein Account mit diesem Namen und diesem Kennwort existiert.
					Es besteht die Möglichkeit, dass Sie sich bei mindestens einer von beiden Angaben vertippt haben, oder ihr Account inzwischen gelöscht wurde.
				</p>
			</div>
		</div>
