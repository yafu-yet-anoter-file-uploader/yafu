			<div id="content">
				<h2>Geheime Frage ändern</h2>
				<?php
				if (isset($notice)) {
					?>
					<div class="">
						<strong>Hinweis:</strong> <?php echo $notice; ?>
					</div>
					<?php
				}
				?>
				<?php
				if (isset($error)) {
					?>
					<div class="">
						<strong>Fehler:</strong> <?php echo $error; ?>
					</div>
					<?php
				}
				?>
				<form action="/user/changeSecret" method="post">
					<p>
						<label>Bisheriges Passwort:</label>
						<input type="password" name="passwordOld" class="w100P" value="">
					</p>
					<p>
						<label>Geheime Frage:</label>
						<input type="text" name="secretQuestion" class="w100P" value="">
					</p>
					<p>
						<label>Geheime Antwort:</label>
						<input type="text" name="secretAnswer" class="w100P" value="">
					</p>
					<p>
						<a href="/user">&lt; Zurück</a> &middot; <input type="submit" value="Geheime Frage ändern">
					</p>
				</form>
			</div>