			<div id="content">
				<div>
					<h3>Registrierung</h3>
					<form action="/user/signup" method="post">
						<?php
							
							if (isset($error)) {
								echo '<p><strong>Fehler:</strong> Die Registrierung ist fehlgeschlagen. Möglicherweise haben Sie sich beim Kennwort vertippt oder der Benutzername ist bereits vergeben</p>';
							}
							
						?>
						<p>
							<label>Gewünschter Benutzername:</label>
						</p>
						<p>
							<input type="text" name="username" class="w100P">
						</p>
						<p>
							<label>Gewünschtes Kennwort:</label>
						</p>
						<p>
							<input type="password" name="password" class="w100P">
						</p>
						<p>
							<label>Gewünschtes Kennwort wiederholen:</label>
						</p>
						<p>
							<input type="password" name="passwordRepetition" class="w100P">
						</p>
						<p>
							<label>Geheime Frage (Optional; für Passwortwiederherstellung):</label>
						</p>
						<p>
							<input type="text" name="secretQuestion" class="w100P">
						</p>
						<p>
							<label>Geheime Antwort (Optional; für Passwortwiederherstellung):</label>
						</p>
						<p>
							<input type="text" name="secretAnswer" class="w100P">
						</p>
						<p>
							<input type="submit" value="Registrierungsformular absenden">
						</p>
					</form>
				</div>
			</div>
