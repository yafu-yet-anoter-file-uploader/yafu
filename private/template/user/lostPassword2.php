			<div id="content">
				<div>
					<h3>Passwort vergessen</h3>
					<form action="/user/lostpassword" method="post">
						<input type="hidden" name="username" value="<?php echo $username; ?>" ?>
						<?php
						if (isset($notice)) {
							?>
							<div class="">
								<strong>Hinweis:</strong> <?php echo $notice; ?>
							</div>
							<?php
						}
						?>
						<?php
						if (isset($error)) {
							?>
							<div class="">
								<strong>Fehler:</strong> <?php echo $error; ?>
							</div>
							<?php
						}
						?>
						<p>
							<label><?php echo $secretQuestion; ?>:</label>
						</p>
						<p>
							<input type="text" name="secretAnswer" class="w100P" value="">
						</p>
						<p>
							<a href="/user">&lt; Zurück</a> &middot; <input type="submit" value="Weiter &gt;">
						</p>
					</form>
				</div>
			</div>
