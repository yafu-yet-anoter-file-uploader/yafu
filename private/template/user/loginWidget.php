				<?php if (!isset($GLOBALS['User'])) { ?>
				<div class="box w350">
					<h2>Anmeldung</h2>
					<form action="/user/login" method="post">
						<p>
							<label>Benutzername:</label><br>
							<input class="w330" type="text" name="username" value="<?php if (isset($usernameSuggestion)) { echo $usernameSuggestion; } ?>">
						</p>
						<p>
							<label>Kennwort:</label><br>
							<input class="w330" type="password" name="password">
						</p>
						<p>
							<input type="checkbox" name="remember" value="true" id="remember">
							<label for="remember">Automatisch anmelden?</label>
						</p>
						<p class="buttonBar">
							<a href="/user/lostPassword">Passwort Vergessen?</a>
							&middot;
							<a href="/user/signup">Registrieren</a>
							&middot;
							<input type="submit" value="Anmelden">
						</p>
					</form>
				</div>
				<?php } else { ?>
				<div class="box w350">
					<h2>Benutzer</h2>
					<p>
						Angemeldet als <strong><?php echo $GLOBALS['User']->username; ?></strong>
					</p>
					<ul>
						<li><a href="/user">Dashboard</a></li>
						<li><a href="/user/logout">Abmelden</a></li>
					</ul>
				</div>
				<?php } ?>
