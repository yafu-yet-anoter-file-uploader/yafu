			<div id="content">
				<div class="box">
					<h2>Datei</h2>
					<div id="preview">
						<?php echo $file->getPreview(); ?>
					</div>
					<table class="niceTable">
						<tr>
							<th colspan="2" class="h1">Datei-Informationen</th>
						</tr>
						<tr>
							<td class="w150 h2">Dateiname:</td>
							<td><?php echo $file->filename; ?></td>
						</tr>
						<tr>
							<td class="w150 h2">Dateigröße:</td>
							<td><?php echo File::getNiceSize($file->filesize); ?></td>
						</tr>
						<tr>
							<td class="w150 h2">Dateityp:</td>
							<td><?php echo File::getNiceMime($file->mime); ?></td>
						</tr>
						<tr>
							<td class="w150 h2">Hochgeladen:</td>
							<td><?php echo date('d.m.Y H:i:s', $file->creationTimestamp); ?></td>
						</tr>
						<tr>
							<td class="w150 h2">Läuft ab:</td>
							<td><?php echo ($file->expireTimestamp != -1) ? date('d.m.Y H:i:s', $file->expireTimestamp) : 'nie'; ?></td>
						</tr>
						<tr>
							<td class="w150 h2">MD5-Prüfsumme:</td>
							<td><?php echo $file->md5; ?></td>
						</tr>
						<tr>
							<td class="w150 h2">SHA1-Prüfsumme:</td>
							<td><?php echo $file->sha1; ?></td>
						</tr>
						<?php if (is_array($file->meta)) { ?>
						<tr>
							<th colspan="2" class="h1">Meta-Informationen</th>
						</tr>
						<?php foreach ($file->meta as $key=>$value) { ?>
						<tr>
							<td class="w150 h2"><?php echo htmlspecialchars($key); ?></td>
							<td><?php echo htmlspecialchars($value); ?></td>
						</tr>
						<?php } ?>
						
						<?php } ?>
						<tr>
							<th colspan="2" class="h1">Teilen</th>
						</tr>
						<tr>
							<td class="w150 h2">Link:</td>
							<td><?php echo htmlspecialchars('http://' . $_SERVER['SERVER_NAME'] . '/show/' . $file->hash); ?></td>
						</tr>
						<tr>
							<td class="w150 h2">BB-Code Link:</td>
							<td><?php echo htmlspecialchars('[url=http://' . $_SERVER['SERVER_NAME'] . '/show/' . $file->hash . ']' . $file->filename . ' (' . File::getNiceSize($file->filesize) . ')' . '[/url]'); ?></td>
						</tr>
						<tr>
							<td class="w150 h2">BB-Code Thumbnail:</td>
							<td><?php echo htmlspecialchars('[url=http://' . $_SERVER['SERVER_NAME'] . '/show/' . $file->hash . '][img]http://' . $_SERVER['SERVER_NAME'] . '/preview/' . $file->hash . '/tiny[/img][/url]'); ?></td>
						</tr>
						<tr>
							<td colspan="2" class="dl">
								<a href="/download/<?php echo $file->getDownloadTicket(); ?>">Download</a>
								<?php
								
								if (isset($file->user) &&  (isset($GLOBALS['User'])) && ($file->user->id == $GLOBALS['User']->id)) {
									
									echo ' &middot; <a href="/show/' . $file->hash . '/delete">Löschen</a>';
									
								}
								
								?>
							</td>
						</tr>
					</table>
				</div>
			</div>
