		<div id="content">
			<div class="flR">
				<?php require_once(__ROOT__ . '/private/template/user/loginWidget.php'); ?>
			</div>
			<div class="box mrR380">
				<h2>Einfügen</h2>
				<p>
					<strong>Fehler:</strong> Der Upload ist fehlgeschlagen. Versuche es noch einmal.
				</p>
				<p>
					Gebe den (Quell-) Text ein, den du mit anderen teilen möchtest.
				</p>
				<form action="/paste/store" method="post" enctype="multipart/form-data">
					<p>
						<textarea name="text" class="w100P h350"></textarea>
					</p>
					<p>
						<input type="submit" value="Einfügen">
					</p>
					<p>
						<a href="#" class="toggle" onclick="toggleOptions(document.getElementById('options00'), this);">
							<span class="icTogglePlus">&nbsp;</span>
							Optionen
						</a>
					</p>
					<div style="display: none;" id="options00">
						<p>
							<label class="w100 dIB">Sichtbarkeit:</label>
							<select class="w300" name="visibility">
								<option value="private">Privat</option>
								<option value="public" selected>Öffentlich</option>
							</select>
						</p>
						<p>
							<label class="w100 dIB">Haltbarkeit:</label>
							<select class="w300" name="durability">
								<option value="10m">10 Minuten</option>
								<option value="30m">30 Minuten</option>
								<option value="1h">1 Stunde</option>
								<option value="12h">12 Stunden</option>
								<option value="1d">1 Tag</option>
								<option value="7d">7 Tage</option>
								<option value="30d" selected>30 Tage</option>
								<option value="365d">365 Tage</option>
								<?php if (isset($GLOBALS['User'])) { ?><option value="inf">Unbegrenzt</option><?php } ?>
							</select>
						</p>
						<p>
							<label class="w100 dIB">Hervorhebung:</label>
							<select class="w300" name="highlighting">
								<optgroup label="Text">
									<option value="plain" selected>Automatisch</option>
									<option value="bb">BB-Code</option>
									<option value="markdown">Markdown</option>
								</optgroup>
								<optgroup label="Quelltext">
									<option value="c">C</option>
									<option value="cpp">C++</option>
									<option value="css">CSS</option>
									<option value="delphi">Delphi</option>
									<option value="doxygen">Doxygen</option>
									<option value="html">HTML</option>
									<option value="java">Java</option>
									<option value="javascript">JavaScript</option>
									<option value="php">PHP</option>
									<option value="qbasic">QBasic</option>
									<option value="sql">SQL</option>
									<option value="vhdl">VHDL</option>
									<option value="web3d">Web3D</option>
								</optgroup>
							</select>
						</p>
					</div>
				</form>
			</div>
		</div>
