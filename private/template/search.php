		<div id="content">
			<div class="flR">
				<?php require_once(__ROOT__ . '/private/template/user/loginWidget.php'); ?>
			</div>
			<div class="box mrR380">
				<h2>Suchen</h2>
				<p>
					Gebe an, wonach du suchst. Dies kann ein <a href="http://de.wikipedia.org/wiki/Multipurpose_Internet_Mail_Extensions">MIME-Dateityp</a> sein (Beispiel: image/png, text/plain), Dateinamen, Tags oder - je nach Dateityp - auch der Inhalt.
					Durchsucht werden sowohl öffentliche Dateien, als auch - wenn du angemeldet bist - Dateien, die du hochgeladen hast.
				</p>
				<form action="/search" method="get" enctype="multipart/form-data">
					<p>
						<input type="text" class="w100P" name="q" value="<?php echo (isset($_GET['q']) ? $_GET['q'] : ''); ?>">
					</p>
					<p>
						<input type="submit" value="Suchen">
					</p>
					<p>
						<a href="#" class="toggle" onclick="toggleOptions(document.getElementById('options00'), this);">
							<span class="icTogglePlus">&nbsp;</span>
							Optionen
						</a>
					</p>
					<div style="display: none;" id="options00">
						<p>
							<input type="checkbox" name="sF" value="true" checked>
							<label for="searchFilename">Dateinamen durchsuchen</label>
						</p>
						<p>
							<input disabled type="checkbox" name="sC" value="true">
							<label for="searchContent">Inhalt durchsuchen</label>
						</p>
						<p>
							<input type="checkbox" name="sM" value="true" checked>
							<label for="searchMime">MIME-Typen durchsuchen</label>
						</p>
						<p>
							<input disabled type="checkbox" name="sT" value="true">
							<label for="searchTags">Tags durchsuchen</label>
						</p>
					</div>
				</form>
			</div>
			<?php if (isset($results['files']) && is_array($results['files'])) { ?>
			<table class="niceTable">
				<tr>
					<th>Dateiname</th>
					<th>Dateityp</th>
					<th>Dateigröße</th>
					<th>Uploader</th>
					<th>Zeitpunkt</th>
				</tr>
				<?php foreach ($results['files'] as $result) { ?>
				<tr>
					<td><a href="/show/<?php echo $result->hash; ?>"><?php echo $result->filename; ?></a></td>
					<td><?php echo File::getNiceMime($result->mime); ?></td>
					<td><?php echo File::getNiceSize($result->filesize); ?></td>
					<td><?php echo ($result->user != NULL ? $result->user->username : 'Anonym'); ?></td>
					<td><?php echo date('d.m.Y H:i:s', $result->creationTimestamp); ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="6">
						<div class="flR">
							<?php for ($i = 0; $i < $results['meta']['pageCount']; $i++) { ?>
								<?php if (($i != 0) && ($i != $results['meta']['pageCount'])) { echo ' &middot; '; } ?>
								<?php if ($i == $url[1]) { ?>
								<?php echo '<b>' . ($i+1) . '</b>'; ?>
								<?php } else { ?>
								<?php echo '<a href="/search/' . $i . '?' . $param . '">' . ($i+1) . '</a>'; ?>
								<?php } ?>
							<?php } ?>
						</div>
						<?php echo $results['meta']['resultCount']; ?> Dateien
					</td>
				</tr>
			</table>
			<?php } elseif (isset($results)) { ?>
			<p>
				Es wurden leider keine Ergebnisse zu dieser Suche gefunden.
			</p>
			<?php } ?>
		</div>
