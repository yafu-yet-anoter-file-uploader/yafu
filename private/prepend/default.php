<?php

define('__ROOT__', realpath(__DIR__ . '/../..'));

// -----------------------------------------------------------------------------

require_once(__ROOT__ . '/private/libs/Database.php');
require_once(__ROOT__ . '/private/libs/DatabaseResult.php');
require_once(__ROOT__ . '/private/libs/File.php');
require_once(__ROOT__ . '/private/libs/News.php');
require_once(__ROOT__ . '/private/libs/TextFormat.php');
require_once(__ROOT__ . '/private/libs/User.php');
require_once(__ROOT__ . '/private/libs/Util.php');

// -----------------------------------------------------------------------------

require_once(__ROOT__ . '/private/prepend/modules/config.php');
//require_once(__ROOT__ . '/private/prepend/modules/error_handler.php');
require_once(__ROOT__ . '/private/prepend/modules/cookies.php');
require_once(__ROOT__ . '/private/prepend/modules/crypt_cookiePassword.php');
require_once(__ROOT__ . '/private/prepend/modules/mysql.php');
require_once(__ROOT__ . '/private/prepend/modules/session.php');
require_once(__ROOT__ . '/private/prepend/modules/parsePath.php');

?>
