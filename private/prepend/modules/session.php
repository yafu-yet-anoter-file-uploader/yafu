<?php

if ($cookiesEnabled) {
	
	session_start();
	
	if (isset($_SESSION['userId'])) {
		
		try {
			
			$GLOBALS['User'] = new User($_SESSION['userId']);
			
		}
		catch (ExceptionUserDoesNotExist $e) {
			
			unset($_SESSION['userId']);
			unset($GLOBALS['User']);
			
		}
		
	}
	else {
		
		if (isset($_COOKIE['username']) && isset($_COOKIE['password'])) {
			
			
			$cookieUsername        = aes_decrypt($_COOKIE['username'], COOKIE_SECRET);
			$cookieSaltAndPassword = aes_decrypt($_COOKIE['password'], COOKIE_SECRET);
			$cookieSalt            = substr($cookieSaltAndPassword, 0, strlen(PASSWORD_SALT));
			$cookiePassword        = substr($cookieSaltAndPassword, strlen(PASSWORD_SALT));
			
			if (!isset($_SESSION['wasLoggedIn']) && ($cookieSalt == PASSWORD_SALT)) {
				
				try {
					
					$_SESSION['userId'] = User::authenticate($cookieUsername, $cookiePassword);
					$GLOBALS['User'] = new User($_SESSION['userId']);
					
				}
				catch (ExceptionLoginIncorrect $e) {
					
					setcookie("username", '', time() + 1, '/');
					setcookie("password", '', time() + 1, '/');
					
				}
				catch (ExceptionUserDoesNotExist $e) {
					
					setcookie("username", '', time() + 1, '/');
					setcookie("password", '', time() + 1, '/');
					
				}
				catch (ExceptionUserIsBanned $e) {
					
					setcookie("username", '', time() + 1, '/');
					setcookie("password", '', time() + 1, '/');
					
				}
				
			}
			else {
				
				$usernameSuggestion = $cookieUsername;
				
			}
			
		}
		
	}
	
}

?>
