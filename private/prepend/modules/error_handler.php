<?php

function errorHandler($code, $text, $file, $line) {
	
	global $url;
	
	$type = 'error';
	require_once(__ROOT__ . '/private/script/error/e500.php');
	
	exit();
}

function exceptionHandler($exception) {
	
	global $url;
	
	$type = 'exception';
	require_once(__ROOT__ . '/private/script/error/e500.php');
	
	exit();
}

set_error_handler('errorHandler');
set_exception_handler('exceptionHandler');

?>
