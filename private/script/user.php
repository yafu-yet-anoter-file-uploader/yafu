<?php

$title = 'Benutzer';

if (!$cookiesEnabled) {
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorNoCookies.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}
else {
	
	switch ($url[1]) {
		case '':
			require_once(__ROOT__ . '/private/script/user/dashboard.php');
			break;
	
		case 'login':
			require_once(__ROOT__ . '/private/script/user/login.php');
			break;
	
		case 'logout':
			require_once(__ROOT__ . '/private/script/user/logout.php');
			break;
	
		case 'signup':
			require_once(__ROOT__ . '/private/script/user/signUp.php');
			break;
	
		case 'lostpassword':
			require_once(__ROOT__ . '/private/script/user/lostPassword.php');
			break;
	
		case 'changepassword':
			require_once(__ROOT__ . '/private/script/user/changePassword.php');
			break;
	
		case 'changesecret':
			require_once(__ROOT__ . '/private/script/user/changeSecret.php');
			break;
	
		case 'delete':
			require_once(__ROOT__ . '/private/script/user/delete.php');
			break;
	
		default:
			require_once(__ROOT__ . '/private/script/error/e404.php');
			break;
	}
}

?>
