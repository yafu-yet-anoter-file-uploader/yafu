<?php

$title = 'Einfügen';

switch ($url[1]) {
	case '':
		require_once(__ROOT__ . '/private/template/generic/head.php');
		require_once(__ROOT__ . '/private/template/generic/navigation.php');
		require_once(__ROOT__ . '/private/template/paste.php');
		require_once(__ROOT__ . '/private/template/generic/tail.php');
		break;
	
	case 'store':
		
		if ((!isset($_POST['text'])) || (trim($_POST['text']) == '') || (!isset($_POST['visibility'])) || (!isset($_POST['durability'])) || (!isset($_POST['highlighting']))) {
			
			Util::redirect('/paste');
			
		}
		
		// Creates tempfile
		$filename = 'paste' . time() . '-' . rand(0, 1000);
		$tempfile = '/tmp/text' . time() . '-' . ip2long($_SERVER['REMOTE_ADDR']);
		file_put_contents($tempfile, $_POST['text']);
		
		// Check visibility
		switch ($_POST['visibility']) {
			case 'private':
				$visibility = File::VISIBILITY_PRIVATE;
				break;
			
			case 'public':
				$visibility = File::VISIBILITY_PUBLIC;
				break;
			
			default:
				Util::redirect('/paste');
				break;
		}
		
		// Check Durability
		switch ($_POST['durability']) {
			case 'inf':
				$durability = File::DURABILITY_INFINITE;
				break;
			
			case '10m':
				$durability = File::DURABILITY_10MINUTES;
				break;
			
			case '30m':
				$durability = File::DURABILITY_30MINUTES;
				break;
			
			case '1h':
				$durability = File::DURABILITY_1HOUR;
				break;
			
			case '12h':
				$durability = File::DURABILITY_12HOURS;
				break;
			
			case '1d':
				$durability = File::DURABILITY_1DAY;
				break;
			
			case '7d':
				$durability = File::DURABILITY_7DAYS;
				break;
			
			case '30d':
				$durability = File::DURABILITY_30DAYS;
				break;
			
			case '365d':
				$durability = File::DURABILITY_365DAYS;
				break;
			
			default:
				Util::redirect('/paste');
				break;
		}
		
		// Check Highlighting
		switch ($_POST['highlighting']) {
			case 'plain':
				$mime = mime_content_type($tempfile);
				
				$highlighting = File::getHighlite($mime);
				$filename    .= File::getExtension($mime);
				
				break;
			
			case 'bb':
				$highlighting = File::HIGHLIGHTING_BBCODE;
				$filename .= '.txt';
				break;
			
			case 'markdown':
				$highlighting = File::HIGHLIGHTING_MARKDOWN;
				$filename .= '.txt';
				break;
			
			case 'c':
				$highlighting = File::HIGHLIGHTING_C;
				$filename .= '.c';
				break;
			
			case 'cpp':
				$highlighting = File::HIGHLIGHTING_CPP;
				$filename .= '.cpp';
				break;
			
			case 'css':
				$highlighting = File::HIGHLIGHTING_CSS;
				$filename .= '.css';
				break;
			
			case 'delphi':
				$highlighting = File::HIGHLIGHTING_DELPHI;
				$filename .= '.pas';
				break;
			
			case 'doxygen':
				$highlighting = File::HIGHLIGHTING_DOXYGEN;
				$filename .= '.dox';
				break;
			
			case 'html':
				$highlighting = File::HIGHLIGHTING_HTML;
				$filename .= '.html';
				break;
			
			case 'java':
				$highlighting = File::HIGHLIGHTING_JAVA;
				$filename .= '.java';
				break;
			
			case 'javascript':
				$highlighting = File::HIGHLIGHTING_JAVASCRIPT;
				$filename .= '.js';
				break;
			
			case 'php':
				$highlighting = File::HIGHLIGHTING_PHP;
				$filename .= '.php';
				break;
			
			case 'qbasic':
				$highlighting = File::HIGHLIGHTING_QBASIC;
				$filename .= '.bas';
				break;
			
			case 'sql':
				$highlighting = File::HIGHLIGHTING_SQL;
				$filename .= '.sql';
				break;
			
			case 'vhdl':
				$highlighting = File::HIGHLIGHTING_VHDL;
				$filename .= '.vhdl';
				break;
			
			case 'web3d':
				$highlighting = File::HIGHLIGHTING_WEB3D;
				$filename .= '.w3d';
				break;
			
			default:
				Util::redirect('/paste');
				break;
		}
		
		try {
			
			$file = File::create($filename, $tempfile, $visibility, $durability, $highlighting);
			Util::redirect('/show/' . $file->hash);
			
		}
		catch (ExceptionFileUploadFailed $e) {
			
			require_once(__ROOT__ . '/private/template/generic/head.php');
			require_once(__ROOT__ . '/private/template/generic/navigation.php');
			require_once(__ROOT__ . '/private/template/pasteError.php');
			require_once(__ROOT__ . '/private/template/generic/tail.php');
			
		}
		
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
}

?>
