<?php

$title = 'Download';

try {
	
	$file = File::getDownload($url[1]);
	$file->download();
	
}
catch (ExceptionFileInexistant $e) {
	
	header('HTTP/1.1 404 Not Found');
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/error/e404.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}

?>