<?php

$title = 'Registrierung';

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['passwordRepetition'])) {
	
	try {
		
		$id = User::create(
			$_POST['username'],
			$_POST['password'],
			$_POST['passwordRepetition'],
			(isset($_POST['secretQuestion']) ? $_POST['secretQuestion'] : ''),
			(isset($_POST['secretAnswer']) ? $_POST['secretAnswer'] : '')
		);
		
		$GLOBALS['User'] = new User($id);
		$_SESSION['userId'] = $id;
		
		require_once(__ROOT__ . '/private/template/generic/head.php');
		require_once(__ROOT__ . '/private/template/generic/navigation.php');
		require_once(__ROOT__ . '/private/template/user/signUpSuccessful.php');
		require_once(__ROOT__ . '/private/template/generic/tail.php');
		
	}
	catch (Exception $e) {
		
		$error = true;
		
		require_once(__ROOT__ . '/private/template/generic/head.php');
		require_once(__ROOT__ . '/private/template/generic/navigation.php');
		require_once(__ROOT__ . '/private/template/user/signUp.php');
		require_once(__ROOT__ . '/private/template/generic/tail.php');
		
	}
	
}
else {
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/signUp.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}

?>
