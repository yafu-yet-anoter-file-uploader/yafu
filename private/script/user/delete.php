<?php

$title = 'Dashboard';

if (isset($GLOBALS['User'])) {
	
	if (isset($_POST['password'])) {
		
		if ($GLOBALS['User']->checkPassword($_POST['password'])) {
			
			$GLOBALS['User']->delete(isset($_POST['deleteFiles']));
			
			unset($_SESSION['userId']);
			unset($GLOBALS['User']);
			
			Util::redirect('/');
			
		}
		else {
			
			$error = 'Falsches Kennwort';
			
		}
		
	}
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/delete.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}
else {

	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorNotLoggedIn.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');

}

?>
