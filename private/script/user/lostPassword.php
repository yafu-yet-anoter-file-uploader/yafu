<?php

$title = 'Dashboard';

if (!isset($GLOBALS['User'])) {
	
	if (!isset($_POST['username'])) {
		
		require_once(__ROOT__ . '/private/template/generic/head.php');
		require_once(__ROOT__ . '/private/template/generic/navigation.php');
		require_once(__ROOT__ . '/private/template/user/lostPassword1.php');
		require_once(__ROOT__ . '/private/template/generic/tail.php');
		
	}
	elseif (isset($_POST['username']) && !isset($_POST['secretAnswer'])) {
		
		try {
			
			$username = $_POST['username'];
			$user = User::getUserByNickname($username);
			
			if ($user->secretQuestion != '') {
				
				$secretQuestion = $user->secretQuestion;
				
				require_once(__ROOT__ . '/private/template/generic/head.php');
				require_once(__ROOT__ . '/private/template/generic/navigation.php');
				require_once(__ROOT__ . '/private/template/user/lostPassword2.php');
				require_once(__ROOT__ . '/private/template/generic/tail.php');
				
			}
			else {
				
				$error = 'Der angegebene Benutzer hat keine Sicherheitsfrage hinterlegt.';
				
				require_once(__ROOT__ . '/private/template/generic/head.php');
				require_once(__ROOT__ . '/private/template/generic/navigation.php');
				require_once(__ROOT__ . '/private/template/user/lostPassword1.php');
				require_once(__ROOT__ . '/private/template/generic/tail.php');
				
				
			}
			
		}
		catch (ExceptionUserDoesNotExist $e) {
			
			$error = 'Ein Benutzer mit diesem Namen existiert nicht im System';
			
			require_once(__ROOT__ . '/private/template/generic/head.php');
			require_once(__ROOT__ . '/private/template/generic/navigation.php');
			require_once(__ROOT__ . '/private/template/user/lostPassword1.php');
			require_once(__ROOT__ . '/private/template/generic/tail.php');
			
		}
		
	}
	elseif (isset($_POST['username']) && isset($_POST['secretAnswer'])) {
		
		try {
			
			$username = $_POST['username'];
			$user = User::getUserByNickname($username);
			
			if ($user->secretQuestion !== null) {
				
				$secretQuestion = $user->secretQuestion;
				
				if ($_POST['secretAnswer'] == $user->secretAnswer) {
					
					$newPassword = '';
					
					$charSet = 'abcdefghijklmnopqrstuvwxyz';
					
					while (strlen($newPassword) != 4) {
						$newPassword .= substr($charSet, rand(0, strlen($charSet)), 1);
					}
					
					$charSet = '0123456789';
					
					while (strlen($newPassword) != 6) {
						$newPassword .= substr($charSet, rand(0, strlen($charSet)), 1);
					}
					
					$user->setPassword($newPassword);
					
					$_SESSION['userId'] = $user->id;
					$GLOBALS['User'] = new User($user->id);
					
					require_once(__ROOT__ . '/private/template/generic/head.php');
					require_once(__ROOT__ . '/private/template/generic/navigation.php');
					require_once(__ROOT__ . '/private/template/user/lostPassword3.php');
					require_once(__ROOT__ . '/private/template/generic/tail.php');
					
				}
				else {
					
					$error = 'Die Antwort ist falsch.';
					
					require_once(__ROOT__ . '/private/template/generic/head.php');
					require_once(__ROOT__ . '/private/template/generic/navigation.php');
					require_once(__ROOT__ . '/private/template/user/lostPassword2.php');
					require_once(__ROOT__ . '/private/template/generic/tail.php');
					
				}
			}
			else {
				
				$error = 'Der angegebene Benutzer hat keine Sicherheitsfrage hinterlegt.';
				
				require_once(__ROOT__ . '/private/template/generic/head.php');
				require_once(__ROOT__ . '/private/template/generic/navigation.php');
				require_once(__ROOT__ . '/private/template/user/lostPassword1.php');
				require_once(__ROOT__ . '/private/template/generic/tail.php');
				
				
			}
			
		}
		catch (ExceptionUserDoesNotExist $e) {
			
			$error = 'Ein Benutzer mit diesem Namen existiert nicht im System';
			
			require_once(__ROOT__ . '/private/template/generic/head.php');
			require_once(__ROOT__ . '/private/template/generic/navigation.php');
			require_once(__ROOT__ . '/private/template/user/lostPassword1.php');
			require_once(__ROOT__ . '/private/template/generic/tail.php');
			
		}
		
	}
	
}
else {

	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/dashboard.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');

}

?>
