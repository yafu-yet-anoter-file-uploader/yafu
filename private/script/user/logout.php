<?php

if (isset($_SESSION['userId'])) {
	
	$_SESSION['wasLoggedIn'] = true;
	unset($_SESSION['userId']);
	unset($GLOBALS['User']);
	
}

Util::redirect('/');

?>
