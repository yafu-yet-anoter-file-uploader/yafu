<?php

$title = 'Dashboard';

if (isset($GLOBALS['User'])) {
	
	if (isset($_POST['secretAnswer']) && isset($_POST['secretAnswer']) && isset($_POST['passwordOld'])) {
		
		if ($GLOBALS['User']->checkPassword($_POST['passwordOld'])) {
			
			$GLOBALS['User']->setSecret($_POST['secretQuestion'], $_POST['secretAnswer']);
			$notice = 'Geheime Frage geändert';
			
		}
		else {
			
			$error = 'Falsches Kennwort';
			
		}
		
	}
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/changeSecret.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}
else {

	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorNotLoggedIn.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');

}

?>
