<?php

if (!isset($_POST['username']) || !isset($_POST['password'])) {
	
	Util::redirect('/');
	
}

if ((trim($_POST['username']) == '') || (trim($_POST['password']) == '')) {
	
	Util::redirect('/');
	
}

try {
	
	$usernameSuggestion = $_POST['username'];
	
	$_SESSION['userId'] = User::authenticate($_POST['username'], $_POST['password']);
	
	if (isset($_POST['remember'])) {
		
		$cookieUsername = aes_encrypt($_POST['username'], COOKIE_SECRET);
		$cookiePassword = aes_encrypt(PASSWORD_SALT . $_POST['password'], COOKIE_SECRET);
		
		setcookie("username", $cookieUsername, time() + (86400 * 365), '/');
		setcookie("password", $cookiePassword, time() + (86400 * 365), '/');
		
	}
	
	Util::redirect('/user');
	
}
catch (ExceptionLoginIncorrect $e) {
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorLoginIncorrect.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}
catch (ExceptionUserDoesNotExist $e) {
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorLoginIncorrect.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}
catch (ExceptionUserIsBanned $e) {
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorUserBanned.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}

?>
