<?php

$title = 'Dashboard';

if (isset($GLOBALS['User'])) {
	
	if (isset($_POST['password']) && isset($_POST['passwordRepetition']) && isset($_POST['passwordOld'])) {
		
		if ($GLOBALS['User']->checkPassword($_POST['passwordOld'])) {
			
			if ($_POST['password'] == $_POST['passwordRepetition']) {
				
				$GLOBALS['User']->setPassword($_POST['password']);
				$notice = 'Passwort geändert';
				
			}
			else {
				
				$error = 'Passwörter stimmen nicht überein.';
				
			}
			
		}
		else {
			
			$error = 'Falsches Kennwort';
			
		}
		
	}
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/changePassword.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}
else {

	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorNotLoggedIn.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');

}

?>
