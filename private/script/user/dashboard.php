<?php

$title = 'Dashboard';

if (isset($GLOBALS['User'])) {
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/dashboard.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	
}
else {
	
	require_once(__ROOT__ . '/private/template/generic/head.php');
	require_once(__ROOT__ . '/private/template/generic/navigation.php');
	require_once(__ROOT__ . '/private/template/user/errorNotLoggedIn.php');
	require_once(__ROOT__ . '/private/template/generic/tail.php');
	

}

?>
