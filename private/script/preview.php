<?php

$title = 'Vorschau';

try {
	
	$file = File::getByHash($url[1]);
	
	if ($url[2] == '') {
		$filename = __ROOT__ . '/private/files/' . $file->id . '_preview';
	}
	elseif ($url[2] == 'tiny') {
		$filename = __ROOT__ . '/private/files/' . $file->id . '_forum';
	}
	else {
		
		require_once(__ROOT__ . '/private/script/error/e404.php');
		
	}
	
	if (isset($filename)) {
		if (file_exists($filename)) {
			
			header('Content-Type: ' . mime_content_type($filename));
			header('Content-Length: ' . filesize($filename));
			readfile($filename);
			
		}
		else {
			
			$file->makePreview();
			
			if (file_exists($filename)) {
				header('Content-Type: ' . mime_content_type($filename));
				header('Content-Length: ' . filesize($filename));
				readfile($filename);
			}
			
		}
	}
	
}
catch (ExceptionFileInexistant $e) {
	
	require_once(__ROOT__ . '/private/script/error/e404.php');
	
}

?>