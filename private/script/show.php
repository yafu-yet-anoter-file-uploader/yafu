<?php

$title = 'Datei';

try {
	
	$file = File::getByHash($url[1]);
	
	if (isset($file->user) && isset($GLOBALS['User']) && ($file->user->id == $GLOBALS['User']->id) && ($url[2] == 'delete') && !isset($_POST['submit'])) {
		
		require_once(__ROOT__ . '/private/template/generic/head.php');
		require_once(__ROOT__ . '/private/template/generic/navigation.php');
		require_once(__ROOT__ . '/private/template/delete.php');
		require_once(__ROOT__ . '/private/template/generic/tail.php');
		
	}
	elseif (isset($file->user) && isset($GLOBALS['User']) && ($file->user->id == $GLOBALS['User']->id) && ($url[2] == 'delete') && isset($_POST['submit'])) {
		
		$file->delete();
		Util::redirect('/');
		
	}
	else {
		
		require_once(__ROOT__ . '/private/template/generic/head.php');
		require_once(__ROOT__ . '/private/template/generic/navigation.php');
		require_once(__ROOT__ . '/private/template/show.php');
		require_once(__ROOT__ . '/private/template/generic/tail.php');
		
	}
}
catch (ExceptionFileInexistant $e) {
	
	require_once(__ROOT__ . '/private/script/error/e404.php');
	
}

?>