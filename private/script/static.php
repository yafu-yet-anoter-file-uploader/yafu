<?php

switch ($url[1]) {
	
	case 'css':
		require_once(__ROOT__ . '/private/script/static/css.php');
		break;
	
	case 'img':
		require_once(__ROOT__ . '/private/script/static/img.php');
		break;
	
	case 'js':
		require_once(__ROOT__ . '/private/script/static/js.php');
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
	
}

?>