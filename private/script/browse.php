<?php

$title = 'Übersicht';

$results = File::browse(intval($url[1]));

require_once(__ROOT__ . '/private/template/generic/head.php');
require_once(__ROOT__ . '/private/template/generic/navigation.php');
require_once(__ROOT__ . '/private/template/browse.php');
require_once(__ROOT__ . '/private/template/generic/tail.php');

?>