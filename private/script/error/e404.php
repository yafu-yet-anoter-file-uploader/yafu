<?php

$title = 'Fehler 404';
header('HTTP/1.1 404 Not Found');

require_once(__ROOT__ . '/private/template/generic/head.php');
require_once(__ROOT__ . '/private/template/generic/navigation.php');
require_once(__ROOT__ . '/private/template/error/e404.php');
require_once(__ROOT__ . '/private/template/generic/tail.php');

?>