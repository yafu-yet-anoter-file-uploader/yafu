<?php

$title = 'Fehler 500';
header('HTTP/1.1 500 Internal Server Error');

require_once(__ROOT__ . '/private/template/generic/head.php');
require_once(__ROOT__ . '/private/template/generic/navigation.php');
require_once(__ROOT__ . '/private/template/error/e500.php');
require_once(__ROOT__ . '/private/template/generic/tail.php');

?>