<?php

$title = 'Startseite';

// -----------------------------------------------------------------------------

$newsEntries = News::getPage(0);

// -----------------------------------------------------------------------------

require_once(__ROOT__ . '/private/template/generic/head.php');
require_once(__ROOT__ . '/private/template/generic/navigation.php');
require_once(__ROOT__ . '/private/template/start.php');
require_once(__ROOT__ . '/private/template/generic/tail.php');

?>
