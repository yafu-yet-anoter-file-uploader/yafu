<?php

$title = 'Hochladen';

switch ($url[1]) {
	case '':
		require_once(__ROOT__ . '/private/template/generic/head.php');
		require_once(__ROOT__ . '/private/template/generic/navigation.php');
		require_once(__ROOT__ . '/private/template/upload.php');
		require_once(__ROOT__ . '/private/template/generic/tail.php');
		break;
	
	case 'status':
		
		if (isset($url[2])) {
			
			if (function_exists('apc_fetch')) {
				
				$status = apc_fetch('upload_' . $url[2]);
				$status['time'] = time();
				echo json_encode($status);
				exit;
				
			}
			else {
				
				echo '-1';
				exit;
				
			}
			
		}
		else {
			
			require_once(__ROOT__ . '/private/script/error/e500.php');
			
		}
		
		break;
	
	case 'store':
		
		if ((count($_FILES) != 1) || (!isset($_POST['visibility'])) || (!isset($_POST['durability']))) {
			
			Util::redirect('/upload');
			
		}
		
		// Check visibility
		switch ($_POST['visibility']) {
			case 'private':
				$visibility = File::VISIBILITY_PRIVATE;
				break;
			
			case 'public':
				$visibility = File::VISIBILITY_PUBLIC;
				break;
			
			default:
				Util::redirect('/upload');
				break;
		}
		
		// Check Durability
		switch ($_POST['durability']) {
			case 'inf':
				$durability = File::DURABILITY_INFINITE;
				break;
			
			case '10m':
				$durability = File::DURABILITY_10MINUTES;
				break;
			
			case '30m':
				$durability = File::DURABILITY_30MINUTES;
				break;
			
			case '1h':
				$durability = File::DURABILITY_1HOUR;
				break;
			
			case '12h':
				$durability = File::DURABILITY_12HOURS;
				break;
			
			case '1d':
				$durability = File::DURABILITY_1DAY;
				break;
			
			case '7d':
				$durability = File::DURABILITY_7DAYS;
				break;
			
			case '30d':
				$durability = File::DURABILITY_30DAYS;
				break;
			
			case '365d':
				$durability = File::DURABILITY_365DAYS;
				break;
			
			default:
				Util::redirect('/upload');
				break;
		}
		
		if (($_FILES['file']['tmp_name'] == '') || ($_FILES['file']['error'] != 0)) {
			
			require_once(__ROOT__ . '/private/template/generic/head.php');
			require_once(__ROOT__ . '/private/template/generic/navigation.php');
			require_once(__ROOT__ . '/private/template/uploadError.php');
			require_once(__ROOT__ . '/private/template/generic/tail.php');
			
		}
		else {
			
			// Check Upload
			$uploadedFileSize = filesize($_FILES['file']['tmp_name']);
			
			if ($uploadedFileSize != intval($_FILES['file']['size'])) {
				
				require_once(__ROOT__ . '/private/template/generic/head.php');
				require_once(__ROOT__ . '/private/template/generic/navigation.php');
				require_once(__ROOT__ . '/private/template/uploadError.php');
				require_once(__ROOT__ . '/private/template/generic/tail.php');
				
			}
			else {
				
				$filename = $_FILES['file']['name'];
				$tempfile = $_FILES['file']['tmp_name'];
				
				$mime = mime_content_type($tempfile);
				$highlighting = File::getHighlite(File::mimeOverride($mime, $filename));
				
				try {
					
					$file = File::create($filename, $tempfile, $visibility, $durability,$highlighting);
					Util::redirect('/show/' . $file->hash);
					
				}
				catch (ExceptionFileUploadFailed $e) {
					
					require_once(__ROOT__ . '/private/template/generic/head.php');
					require_once(__ROOT__ . '/private/template/generic/navigation.php');
					require_once(__ROOT__ . '/private/template/uploadError.php');
					require_once(__ROOT__ . '/private/template/generic/tail.php');
					
				}
				
				
			}
			
		}
		
		
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
}

?>
