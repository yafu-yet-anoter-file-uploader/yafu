<?php

header('Content-Type: text/plain; charset=utf8');
echo 'Running Cron -------------------------------------------------------------------' . "\n\n";
echo 'Server Time: ' . date('Y-m-d H:i:s') . "\n\n";

echo 'Searching for expired files...' . "\n\n";
$files = File::getExpiredFiles();
echo count($files) . ' file(s) found.' . "\n";

foreach ($files as $file) {
	
	echo ' - deleting ' . $file->filename . ' (#' . $file->id . ')' .  "\n";
	$file->delete();
	
}

echo "\n";
echo 'Searching for expired tickets...' . "\n\n";
$tickets = File::deleteExpiredTickets();

echo count($tickets) . ' ticket(s) found.' . "\n";

foreach ($tickets as $ticket) {
	
	echo ' - deleted ' . $ticket .  "\n";
	
}

?>