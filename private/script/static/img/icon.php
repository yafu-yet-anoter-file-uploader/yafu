<?php

switch ($url[3]) {
	
	case 'toggleminus':
		header('content-type: image/png');
		readfile(__ROOT__ . '/ext/famfamfam silk/icons/bullet_toggle_minus.png');
		break;
	
	case 'toggleplus':
		header('content-type: image/png');
		readfile(__ROOT__ . '/ext/famfamfam silk/icons/bullet_toggle_plus.png');
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
	
}

?>
