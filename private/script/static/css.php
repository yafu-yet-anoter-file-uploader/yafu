<?php

switch ($url[2]) {
	
	case 'generic':
		header('content-type: text/css; charset=utf-8');
		readfile(__ROOT__ . '/private/static/stylesheet/generic.css');
		break;
	
	case 'specific':
		header('content-type: text/css; charset=utf-8');
		readfile(__ROOT__ . '/private/static/stylesheet/specific.css');
		break;
	
	case 'icon':
		header('content-type: text/css; charset=utf-8');
		readfile(__ROOT__ . '/private/static/stylesheet/icon.css');
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
	
}

?>