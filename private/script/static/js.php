<?php

switch ($url[2]) {
	
	case 'util':
		header('content-type: application/x-javascript');
		readfile(__ROOT__ . '/private/static/javascript/util.js');
		break;
	
	case 'json':
		header('content-type: application/x-javascript');
		readfile(__ROOT__ . '/ext/json.js');
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
	
}

?>