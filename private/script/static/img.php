<?php

switch ($url[2]) {
	
	case 'logo':
		header('content-type: image/png');
		readfile(__ROOT__ . '/private/static/images/yafulogo.png');
		break;
	
	case 'icon':
		require_once(__ROOT__ . '/private/script/static/img/icon.php');
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
	
}

?>