<?php

$title = 'News-Archiv';

// -----------------------------------------------------------------------------

$newsEntries = News::getPage(intval($url[1]));
$newsPageCount = News::getPageCount();

// -----------------------------------------------------------------------------

require_once(__ROOT__ . '/private/template/generic/head.php');
require_once(__ROOT__ . '/private/template/generic/navigation.php');
require_once(__ROOT__ . '/private/template/news.php');
require_once(__ROOT__ . '/private/template/generic/tail.php');

?>
