<?php
// -----------------------------------------------------------------------------
require_once('../private/prepend/default.php');
// -----------------------------------------------------------------------------

switch ($url[0]) {
	case '':
	case 'start':
		require_once(__ROOT__ . '/private/script/start.php');
		break;
	
	case 'upload':
		require_once(__ROOT__ . '/private/script/upload.php');
		break;
	
	case 'paste':
		require_once(__ROOT__ . '/private/script/paste.php');
		break;
	
	case 'search':
		require_once(__ROOT__ . '/private/script/search.php');
		break;
	
	case 'browse':
		require_once(__ROOT__ . '/private/script/browse.php');
		break;
	
	case 'info':
		require_once(__ROOT__ . '/private/script/info.php');
		break;
	
	case 'show':
		require_once(__ROOT__ . '/private/script/show.php');
		break;
	
	case 'preview':
		require_once(__ROOT__ . '/private/script/preview.php');
		break;
	
	case 'cron':
		require_once(__ROOT__ . '/private/script/cron.php');
		break;
	
	case 'download':
		require_once(__ROOT__ . '/private/script/download.php');
		break;
	
	case 'help':
		require_once(__ROOT__ . '/private/script/help.php');
		break;
	
	case 'news':
		require_once(__ROOT__ . '/private/script/news.php');
		break;
	
	case 'user':
		require_once(__ROOT__ . '/private/script/user.php');
		break;
	
	case 'st':
		require_once(__ROOT__ . '/private/script/static.php');
		break;
	
	default:
		require_once(__ROOT__ . '/private/script/error/e404.php');
		break;
}

// -----------------------------------------------------------------------------
require_once('../private/append/default.php');
// -----------------------------------------------------------------------------
?>
