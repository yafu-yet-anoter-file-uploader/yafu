/******************************************************************************
 * YAFU CORE DATABASE PATCH                                                   *
 ******************************************************************************/

CREATE TABLE IF NOT EXISTS `file` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`userId` int(11) DEFAULT NULL,
	`folderId` int(11) DEFAULT NULL,
	`mimetype` varchar(255) NOT NULL,
	`filename` varchar(255) NOT NULL,
	`filesize` int(11) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	`expireTimestamp` int(11) DEFAULT NULL,
	`visibility` tinyint(4) NOT NULL,
	`durability` varchar(10) NOT NULL,
	`highlighting` varchar(10) NOT NULL,
	`uploaderIp` int(11) NOT NULL,
	`uploaderReferer` int(11) DEFAULT NULL,
	`uploaderAgent` int(11) DEFAULT NULL,
	`sha1` varchar(40) NOT NULL,
	`md5` varchar(32) NOT NULL,
	`hash` varchar(40) NOT NULL,
	
	PRIMARY KEY (`id`),
	KEY `filename` (`filename`),
	KEY `folderId` (`folderId`),
	KEY `userId` (`userId`),
	KEY `mimetype` (`mimetype`),
	KEY `uploaderReferer` (`uploaderReferer`),
	KEY `uploaderAgent` (`uploaderAgent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `file-tag` (
	`fileId` int(11) NOT NULL,
	`tagId` int(11) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	
	PRIMARY KEY (`fileId`,`tagId`),
	KEY `tagId` (`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `file-ticket` (
	`ticketId` varchar(32) NOT NULL,
	`ip` int(11) NOT NULL,
	`fileId` int(11) NOT NULL,
	`requestTimestamp` int(11) NOT NULL,
	
	PRIMARY KEY (`ticketId`,`ip`),
	KEY `fileId` (`fileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `folder` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`userId` int(11) NOT NULL,
	`name` varchar(255) NOT NULL,
	`parent` int(11) DEFAULT NULL,

	PRIMARY KEY (`id`),
	KEY `userId` (`userId`),
	KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `news` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`creationTimestamp` int(11) NOT NULL,
	`author` int(11) NULL,
	`title` varchar(255) NOT NULL,
	`text` text NOT NULL,
	
	PRIMARY KEY (`id`),
	KEY `author` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `tag` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`label` varchar(32) NOT NULL,
	`count` int(11) NOT NULL,
	`hits` int(11) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	`lastUpdateTimestamp` int(11) NOT NULL,
	`lastHitTimestamp` int(11) NOT NULL,
	
	PRIMARY KEY (`id`),
	KEY `label` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `user` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(32) NOT NULL,
	`password` varchar(32) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	`lastLoginTimestamp` int(11) NULL,
	`license` int(11) NOT NULL,
	`secretQuestion` varchar(255) DEFAULT NULL,
	`secretAnswer` varchar(255) NOT NULL,
	
	PRIMARY KEY (`id`),
	UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `userAgent` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`agent` varchar(255) NOT NULL,
	`hits` int(11) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	`lastHitTimestamp` int(11) NOT NULL,
	
	PRIMARY KEY (`id`),
	UNIQUE KEY `agent` (`agent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `userHit` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`url` varchar(255) NOT NULL,
	`hits` int(11) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	`lastHitTimestamp` int(11) NOT NULL,
	
	PRIMARY KEY (`id`),
	UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `userIp` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`ip` int(11) NOT NULL,
	`hits` int(11) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	`lastHitTimestamp` int(11) NOT NULL,
	
	PRIMARY KEY (`id`),
	UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

CREATE TABLE IF NOT EXISTS `userReferer` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`referer` varchar(255) NOT NULL,
	`hits` int(11) NOT NULL,
	`creationTimestamp` int(11) NOT NULL,
	`lastHitTimestamp` int(11) NOT NULL,
	
	PRIMARY KEY (`id`),
	UNIQUE KEY `referer` (`referer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

ALTER TABLE `file`
	ADD CONSTRAINT `file_ibfk_6` FOREIGN KEY (`uploaderAgent`) REFERENCES `userAgent` (`id`) ON DELETE SET NULL,
	ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE SET NULL,
	ADD CONSTRAINT `file_ibfk_2` FOREIGN KEY (`folderId`) REFERENCES `folder` (`id`) ON DELETE SET NULL,
	ADD CONSTRAINT `file_ibfk_5` FOREIGN KEY (`uploaderReferer`) REFERENCES `userReferer` (`id`) ON DELETE SET NULL;

--

ALTER TABLE `file-tag`
	ADD CONSTRAINT `file@002dtag_ibfk_2` FOREIGN KEY (`tagId`) REFERENCES `tag` (`id`) ON DELETE CASCADE,
	ADD CONSTRAINT `file@002dtag_ibfk_1` FOREIGN KEY (`fileId`) REFERENCES `file` (`id`) ON DELETE CASCADE;

--

ALTER TABLE `file-ticket`
	ADD CONSTRAINT `file@002dticket_ibfk_1` FOREIGN KEY (`fileId`) REFERENCES `file` (`id`) ON DELETE CASCADE;

--

ALTER TABLE `folder`
	ADD CONSTRAINT `folder_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `folder` (`id`) ON DELETE CASCADE,
	ADD CONSTRAINT `folder_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--

ALTER TABLE `news`
	ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`author`) REFERENCES `user` (`id`) ON DELETE SET NULL;


/******************************************************************************/

INSERT
	INTO
		`user` 
		(
			`id` , `username` , `password` , `creationTimestamp` ,
			`license` , `secretQuestion` , `secretAnswer`
		)
	VALUES
		(
			NULL , 'admin', MD5('admin') , UNIX_TIMESTAMP() ,
			65535, NULL , ''
		);

INSERT
	INTO
		`news`
		(
			`id`, `creationTimestamp`, `author`, `title`,
			`text`
		)
	VALUES
		(
			NULL, UNIX_TIMESTAMP(), 1, 'YAFU wurde erfolgreich installiert',
			'Das YAFU-System auf diesem Server wurde erfolgreich installiert und kann ab sofort genutzt werden.'
		);
